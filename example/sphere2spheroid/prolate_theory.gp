#prolate with axis length a=b, c
c=1
#using x to represent a

e(x)=sqrt(1-x*x/(c*c))
A(x)=2.0*pi*x*x*(1+c*asin(e(x))/x/e(x))
Vs(x)=4.0*pi*(A(x)/4.0/pi)**(3.0/2.0)/3.0
V(x)=4.0*pi*x*x*c/3.0

plot [0:] Vs(x), V(x)