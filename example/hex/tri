#!/bin/sh

tri.awk util merge off stat -e '
function add(x0, y0, x1, y1, x2, y2,    i) {
    i = _i
    u0[i] = x0; v0[i] = y0; w0[i] = 0
    u1[i] = x1; v1[i] = y1; w1[i] = 0
    u2[i] = x2; v2[i] = y2; w2[i] = 0
    _i++
}

function add_d(x1, y1, x2, y2,   dx, dy) {
    add(dx, dy,   x1 + dx, y1 + dy,   x2 + dx, y2 + dy)
}

function ini() {
    prog = "hex/main"
    _i = 0
    pi = 3.141592653589793
    R = 1
    h = R*sin(pi/3); lo = -2*pi/3
    n = 4
}

function coord(   i, phi) {
    for (i = 0; i < n + 1; i++)
	phi[i] = lo + i*pi/3
    for (i = 0; i < n + 1; i++) {
	x[i] = R*cos(phi[i])
	y[i] = R*sin(phi[i])
    }
}

function full(dx, dy,   i) {
    for (i = 0; i < n; i++)
	add_d(x[i], y[i],   x[i+1], y[i+1],    dx, dy)
}

function rigth(dx, dy, t) {
    t = x[2]; x[2] /= 2; full(dx, dy); x[2] = t
}

function left(dx, dy, t0, t4) {
    t0 = x[0]; t4 = x[4]
    x[0] = 0; x[4] = 0
    full(dx, dy)
    x[0] = t0; x[4] = t4
}

function standartize(xx, yy, zz,   xc, yc, zc, L, i) {
    xc = stat_mean(xx)
    yc = stat_mean(yy)
    zc = stat_mean(zz)
    L = stat_max(xx) - stat_min(xx)
    for (i in xx) {
	xx[i] -= xc
	yy[i] -= yc
	zz[i] -= zc
    }

    for (i in xx) {
	xx[i] /= L
	yy[i] /= L
	zz[i] /= L
    }
}

function usg() {
    ms0(sprintf("%s n m > OFF", prog))
    ms0("create triangulated n x m plain")
    exit 2
}

function arg() {
    prog = "hex/tri"
    if (noarg()) usg()
    N = ARGV[1]; shift()

    if (noarg()) usg()
    M = ARGV[1]; shift()
}

BEGIN {
    arg()
    ini()
    coord()

    for (j = 0; j < M; j++)
    for (i = 0; i < N; i++) {
	yc = j * 2*h
	xc = i * R
	if (i == 0) left(xc, yc)
	else if (i == N - 1) rigth(xc, yc)
	else full(xc, yc)
    }

    n = merge(u0, v0, w0, u1, v1, w1, u2, v2, w2,   xx, yy, zz, t0, t1, t2)
    standartize(xx, yy, zz)
    off_write0("/dev/stdout", xx, yy, zz, t0, t1, t2)
}
' -- "$@"
