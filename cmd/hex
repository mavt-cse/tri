#!/bin/sh

tri.awk util off math -e '
function usg() {
    printf "%s cx cy deg > OFF\n", prog | "cat >&2"
    printf "bend a radious one hexagon\n" | "cat >&2"
    printf "cx, cy:  principle curvatures\n" | "cat >&2"
    printf "deg: angle of rotation in degree\n" | "cat >&2"
    printf "Example:\n" | "cat >&2"
    printf "%s 1 2 30 > q.off\n", prog | "cat >&2"
    exit
}

function ini() {
    prog = "tri.hex"
    pi = 3.141592653589793
    n = 6; lo = 0; hi = 2*pi
}

function arg() {
    if (eq(ARGV[1], "-h"))
	usg()
    if (noarg()) err("expect cx")
    cx = ARGV[1]; shift()
    if (noarg()) err("expect cy")
    cy = ARGV[1]; shift()
    if (noarg()) err("expect deg")
    ang = ARGV[1]; shift()
    ang = deg2rad(ang)
}

function zz(x, y) { return cx*x^2 + cy*y^2 }

function main(   m, phi) {
    vert(y = 0, x = 0, z = 0) # first
    for (m = 0; m < n; m++) {
	phi = lo + m*(hi - lo)/n
	x = cos(phi + ang); y = sin(phi + ang)
	vert(x, y, zz(x, y))
    }
    for (m = 0; m < n - 1; m++)
	tri(i = 0, m + 1, m + 2)
    tri(i = 0, n, 1)
    off_write()
}

function vert(x, y, z) { NV += 0
    XX[NV] = x
    YY[NV] = y
    ZZ[NV] = z
    NV++
}

function tri(i, j, k) { NT += 0
    T0[NT] = i; T1[NT] = j; T2[NT] = k
    NT++
}

BEGIN {
    ini()
    arg()
    main()
}' -- "$@"
