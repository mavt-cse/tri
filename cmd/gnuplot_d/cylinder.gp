set term x11
set key off
set view equal
set ticslevel 0
set xrange [-6:6]
set yrange [-6:6]
set zrange [-6:6]
set parametric
R = 2; xc = 2; yc = 3
splot "-", "-", u, xc + R*cos(v), yc + R*sin(v)
