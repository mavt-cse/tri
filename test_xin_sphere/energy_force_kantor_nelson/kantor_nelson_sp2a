#!/bin/sh
#############################################################
#We calculate the bending energy using Kantor-Nelson model.
#More precisely, H^2, where H=(c_1+c_2)/2 is the mean curvature.
#Therefore, the coefficient in the formula
#may or may not be the same as the original paper.
#
#Reference: Kantor & Nelson, Phys. Rev. Lett 1987 and Phys. Rev. A 1987
#
##Note there is an ambiguity to split the energy calculated
#into the neigboring vertices.
#Here it splits into 2 times 1/2 for each vertex opposite to the edge.
#############################################################
tri.awk util off setup tri vec -e '
BEGIN {
    prog = "kantor_nelson_sp2a"
    if (noarg()) err("needs OFF file")
    file_in = ARGV[1]; shift()
    file_out1=ARGV[1]; shift()
    file_out2=ARGV[1]; shift()
    #print file_in, file_out1, file_out2

    off_read(file_in)
    setup()

    eng_tot=0
    coef=sqrt(3.0)
    X = 0; Y = 1; Z = 2
    for (m = 0; m < NE; m++) {
        if (m in EDG_BND) continue
	i = D0[m]; j = D1[m]; k = D2[m]; l = D3[m]
	a[X] = XX[i]; a[Y] = YY[i]; a[Z] = ZZ[i]
	b[X] = XX[j]; b[Y] = YY[j]; b[Z] = ZZ[j]
	c[X] = XX[k]; c[Y] = YY[k]; c[Z] = ZZ[k]
	d[X] = XX[l]; d[Y] = YY[l]; d[Z] = ZZ[l]
	ang = tri_dih(a, b, c, d)
	eng = coef*(1 - cos(ang))
	eng_tot+=eng
	eng /= 2
	E[i] += eng; E[l] += eng
    }
    
    print "#axis distance:energy"
    print "#axis distance:energy" > file_out1

    for (m = 0; m < NV; m++) {
	x = XX[m]; y = YY[m]; r = sqrt(x^2 + y^2)
	print r, E[m]
	print r, E[m] >> file_out1
    }

    print "#total energy:", eng_tot
    print "#total energy:", eng_tot > file_out1

    print "#coord x:y:z:energy"
    print "#coord x:y:z:energy" > file_out2

    for (m = 0; m < NV; m++) {
	x = XX[m]; y = YY[m]; 
	print XX[m], YY[m], ZZ[m], E[m]
	print XX[m], YY[m], ZZ[m], E[m] >> file_out2

    }

    print "#total energy:", eng_tot
    print "#total energy:", eng_tot > file_out2

}' -- "$@"
