function rbc_ini0() {
    RBC_P["a"] = 0.54353; RBC_P["b"] = 0.121435;  RBC_P["c"] = -0.561365
}

function rbc_z(x, y, z,   cu, u, r, z0, eps) {
    # 'z coordinate at 'x, 'y; 'z in input sets direction
    eps = 1e-6
    rbc_ini0()
    r = sqrt(x^2 + y^2)
    if (r > 1 + eps)
	err(sprintf("rbc_z: r=%s > 1 for x = %s, y = %s", r, x, y))
    r = min(r, 1)
    cu = sqrt(1-r^2)
    z0 = rbc_F0(cu)
    return copy_sign(z0, z)
}

function rbc_z0(x, y, z,   cu, u, r, z0, eps) {
    eps = 1e-6
    rbc_ini0()
    r = sqrt(x^2 + y^2)
    if (r > 1 + eps)
	err(sprintf("rbc_z: r=%s > 1 for x = %s, y = %s", r, x, y))
    r = min(r, 1)
    cu = sqrt(1-r^2)
    z0 = rbc_F00(cu)
    return copy_sign(z0, z)
}

function rbc_normal(x, y, z,   n, cv, sv, su, cu, r, g_su2, eps) {
    # normal at 'x, 'y; 'z sets direction
    eps = 1e-6    
    X = 0; Y = 1; Z = 2
    rbc_ini0()
    r = sqrt(x^2 + y^2)
    if (r > 1 + eps)
	err(sprintf("rbc_normal: r=%s > 1 for x = %s, y = %s", r, x, y))
    r = min(r, 1)
    # cos(v), sin(v), sin(v), cos(v)
    if (r != 0) { cv = x/r; sv = y/r }
    else        { cv = sv = 0 }
    su = r; cu = sqrt(1-r^2)
    g_su2 = rbc_F1(cu)^2*su^2+cu^2
    n[X] = (rbc_F1(cu)*cv*su)/sqrt(g_su2)
    n[Y] = (rbc_F1(cu)*su*sv)/sqrt(g_su2)
    n[Z] = copy_sign(cu/sqrt(g_su2), z)
}

function rbc_invF00(z) { # invert one term of F0
    rbc_ini0()
    a = RBC_P["a"]
    return z/a
}
function rbc_F00(q) { # one term of F0
    a = RBC_P["a"]
    return a*q
}
function rbc_F0(q) {
    a = RBC_P["a"]; b = RBC_P["b"]; c = RBC_P["c"]
    return c*q^5+b*q^3+a*q
}
function rbc_F1(q, p,   a, b, c) {
    a = RBC_P["a"]; b = RBC_P["b"]; c = RBC_P["c"]
    return 5*c*q^4+3*b*q^2+a
}
function rbc_F2(q, p,   a, b, c) {
    a = RBC_P["a"]; b = RBC_P["b"]; c = RBC_P["c"]
    return 20*c*q^3+6*b*q
}
