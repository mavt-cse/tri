function stat_any(a,   i) { # any element of a
    for (i in a)
	return a[i]
    err("[stat_any] a is empty")
}

function stat_min(a,   m, i, c) {
    m = stat_any(a)
    for (i in a) {
	c = a[i]
	if (c < m) m = c
    }
    return m
}

function stat_max(a,   m, i, c) {
    m = stat_any(a)
    for (i in a) {
	c = a[i]
	if (c > m) m = c
    }
    return m
}

function stat_mean(a,   i, s, n) {
    for (i in a) {
	s += a[i]
	n ++
    }
    if (n == 0) err("[stat_mean] a is empty")
    return s/n
}

function stat_var(a,   i, m, s, n) {
    m = stat_mean(a)
    for (i in a) {
	s += (a[i] - m)^2
	n ++
    }
    return s/n
}

function stat_sum(a,   i, s) {
    for (i in a) s += a[i]
    return s
}

function stat_sum_sq(a,   i, s) {
    for (i in a) s += a[i]^2
    return s
}
