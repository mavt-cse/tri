function gompper_deng(fx, fy, fz, A,
		      h, n, nn, i, j, k, cot, T, l2, a, ac, b, bc, ca, da, db, dc,
		      f, ll, ik, ji, jk, ki, m, r, v, c, co, X, Y, Z, nv) {
    nv = util_nelem(XX)
    X = 0; Y = 1; Z = 2
    for (h = 0; h < NH; h++) {
	n = nxt(h); nn = nxt(n); f = flp(h)
	i = ver(h); j = ver(n); k = ver(nn)
	get3(i, j, k,   a, b, c)
	co = tri_ctan0(b, c, a)
	TT[h] += co; TT[f] += co
    }
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	get_edg(i, j, r)
	r2 = vec_dot(r, r)
	T = TT[h]
	A[i]  += T*r2/8
	LX[i] += T*r[X]/2; LY[i] += T*r[Y]/2; LZ[i] += T*r[Z]/2
    }
    for (m = 0; m < nv; m++) { LX[m] /= A[m]; LY[m] /= A[m]; LZ[m] /= A[m] }
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	T = TT[h]
	get_edg(i, j,   r)
	vec_get(i, LX, LY, LZ, ll)
	l2 = vec_dot(ll, ll)
	vec_linear_combination(T/2, ll, -T*l2/8, r,  df)
	vec_append(df, i,   fx, fy, fz)
	vec_substr(df, j,   fx, fy, fz)
    }
    for (h = 0; h < NH; h++) {
	n = nxt(h); nn = nxt(n)
	i = ver(h); j = ver(n); k = ver(nn)
	get3(i, j, k,   a, b, c)
	dtri_ctan0(a, b, c,    da, db, dc)
	get_edg(i, k, r)
	vec_get(k, LX, LY, LZ, lk)
	vec_get(i, LX, LY, LZ, li)
	r2 = vec_dot(r, r)
	dl = vec_dot(lk, lk) + vec_dot(li, li)
	dd = vec_dot(li, r)  - vec_dot(lk, r)
	C = dd/2 - r2*dl/16
	vec_scalar_append(da,  C,  i, fx, fy, fz)
	vec_scalar_append(db,  C,  j, fx, fy, fz)
	vec_scalar_append(dc,  C,  k, fx, fy, fz)
    }
    msg(fx[0] " " fy[0] " " fz[0])
}

function gompper_dh(fx, fy, fz, A,
		    h, n, nn, i, j, k, cot, T, l2, a, ac, b, bc, ca, da, db, dc,
		    f, ll, ik, ji, jk, ki, m, r, v, c, co, X, Y, Z, nv) {
    nv = util_nelem(XX)
    X = 0; Y = 1; Z = 2
    for (h = 0; h < NH; h++) {
	n = nxt(h); nn = nxt(n); f = flp(h)
	i = ver(h); j = ver(n); k = ver(nn)
	get3(i, j, k,   a, b, c)
	co = tri_ctan0(b, c, a)
	TT[h] += co; TT[f] += co
    }
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	get_edg(i, j, r)
	r2 = vec_dot(r, r)
	T = TT[h]
	A[i]  += T*r2/8
	LX[i] += T*r[X]/2; LY[i] += T*r[Y]/2; LZ[i] += T*r[Z]/2
    }

    for (m = 0; m < nv; m++) { LX[m] /= A[m]; LY[m] /= A[m]; LZ[m] /= A[m] }
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	T = TT[h]
	get_edg(i, j,   r)
	vec_get(i, LX, LY, LZ, ll)
	l2 = vec_dot(ll, ll)
	vec_linear_combination(T/2, ll, -T*l2/8, r,  df)
	vec_append(df, i,   fx, fy, fz)
	vec_substr(df, j,   fx, fy, fz)
    }
    for (h = 0; h < NH; h++) {
	n = nxt(h); nn = nxt(n)
	i = ver(h); j = ver(n); k = ver(nn)
	get3(i, j, k,   a, b, c)
	dtri_ctan0(a, b, c,    da, db, dc)
	get_edg(i, k, r)
	vec_get(k, LX, LY, LZ, lk)
	vec_get(i, LX, LY, LZ, li)
	r2 = vec_dot(r, r)
	dl = vec_dot(lk, lk) + vec_dot(li, li)
	dd = vec_dot(li, r)  - vec_dot(lk, r)
	C = dd/2 - r2*dl/16
	vec_scalar_append(da,  C,  i, fx, fy, fz)
	vec_scalar_append(db,  C,  j, fx, fy, fz)
	vec_scalar_append(dc,  C,  k, fx, fy, fz)
    }
    msg(fx[0] " " fy[0] " " fz[0])
}
