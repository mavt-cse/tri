function surf_t(T,   h, n, nn, f, i, j, k, a, b, c, co) {
    delete T
    if (NH == 0) err("[surf_T] NH == 0")
    for (h = 0; h < NH; h++) {
	n = nxt(h); nn = nxt(n)
	i = ver(h); j = ver(n); k = ver(nn)
	get3(i, j, k,   a, b, c)
	co = tri_ctan0(b, c, a)
	T[h] += co
	if (!h_bnd(h)) {
	    f = flp(h)
	    T[f] += co
	}
    }
}

function surf_voronoi(T,  A,   h, n, i, j, r, r2, T0) {
    if (NH == 0) err("[surf_T] NH == 0")
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	get_edg(i, j, r)
	r2 = vec_dot(r, r)
	T0 = T[h]
	A[i]  += T0*r2/8
    }
}

function surf_laplace(T, A, V, L,   h, n, i, j, r, r2, m) {
    if (NH == 0) err("[surf_T] NH == 0")
    for (h = 0; h < NH; h++) {
	n = nxt(h); i = ver(h); j = ver(n)
	get_edg(i, j, r)
	r2 = vec_dot(r, r)
	L[i] += T[h]*(V[i] - V[j])/2
    }
    for (m = 0; m in L; m++) L[m] /= A[m]
}

function surf_curv_mean(curv,   t, a, lx, ly, lz, m) {
    surf_t(   t)
    surf_voronoi(t,   a)
    surf_laplace(t, a, XX, lx)
    surf_laplace(t, a, YY, ly)
    surf_laplace(t, a, ZZ, lz)
    for (m in lx) {
	curv[m] = sqrt(lx[m]^2 + ly[m]^2 + lz[m]^2)/2
    }
}
