function vec_get(i, xx, yy, zz,   a,   X, Y, Z) {
    if (!arrayp(xx)) err_push("[vec_get] xx is not an array")
    if (!numberp(i)) err_push(sprintf("[vec_get] '%s' is not a number", i))
    X = 0; Y = 1; Z = 2
    a[X] = xx[i]; a[Y] = yy[i]; a[Z] = zz[i]
}

function vec_append(a, i, xx, yy, zz,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    if (emptyp(i)) err("[vec_append] emptyp(i)")
    xx[i] += a[X]; yy[i] += a[Y]; zz[i] += a[Z]
}

function vec_substr(a, i, xx, yy, zz,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    xx[i] -= a[X]; yy[i] -= a[Y]; zz[i] -= a[Z]
}

function vec_scalar_append(a, s, i, xx, yy, zz,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    xx[i] += s*a[X]; yy[i] += s*a[Y]; zz[i] += s*a[Z]    
}

function vec_set(a, i, xx, yy, zz,   X, Y, Z) {
   X = 0; Y = 1; Z = 2
   xx[i] = a[X]; yy[i] = a[Y]; zz[i] = a[Z]
}

function vec_scalar(a, s,   b,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    b[X] = s*a[X]; b[Y] = s*a[Y]; b[Z] = s*a[Z]
}

function vec_negative(a,   b) { vec_scalar(a, -1,   b) }
function vec_zero(a,   X, Y, Z) {
    X = 0; Y = 1; Z = 2;
    a[X] = a[Y] = a[Z] = 0
}

function vec_minus(a, b, ab,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    if (!(Y in a)) err_push("[vec_minus] a is empty")
    if (!(Y in b)) err_push("[vec_minus] b is empty")
    ab[X] = a[X] - b[X]
    ab[Y] = a[Y] - b[Y]
    ab[Z] = a[Z] - b[Z]
}

function vec_plus(a, b, ab,    X, Y, Z) {
    X = 0; Y = 1; Z = 2
    if (!(Y in a)) err_push("vector a is empty")
    if (!(Y in b)) err_push("vector b is empty")
    ab[X] = a[X] + b[X]
    ab[Y] = a[Y] + b[Y]
    ab[Z] = a[Z] + b[Z]
}

function vec_mean2(a, b, ab,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    ab[X] = (a[X] + b[X])/2
    ab[Y] = (a[Y] + b[Y])/2
    ab[Z] = (a[Z] + b[Z])/2
}

function vec_mean3(a, b, c,   abc, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    abc[X] = (a[X] + b[X] + c[X])/3
    abc[Y] = (a[Y] + b[Y] + c[Y])/3
    abc[Z] = (a[Z] + b[Z] + c[Z])/3
}

function vec_cross(a, b, c,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    c[X] = a[Y]*b[Z]-b[Y]*a[Z]
    c[Y] = b[X]*a[Z]-a[X]*b[Z]
    c[Z] = a[X]*b[Y]-b[X]*a[Y]
}

function vec_linear_combination(al, a, be, b,   c,  X, Y, Z) { # alpha*a + beta*b
    X = 0; Y = 1; Z = 2
    c[X] = al*a[X] + be*b[X]
    c[Y] = al*a[Y] + be*b[Y]
    c[Z] = al*a[Z] + be*b[Z]
}

function vec_dot(a, b,    X, Y, Z) {
    X = 0; Y = 1; Z = 2
    return a[X]*b[X] + a[Y]*b[Y] + a[Z]*b[Z]
}

function vec_abs(a,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    return sqrt(a[X]*a[X] + a[Y]*a[Y] + a[Z]*a[Z])
}

function vec_angle(a, b, n,   y, x) {
    vec_cross(a, b, n)
    y = vec_abs(n)
    x = vec_dot(a, b)
    return atan2_pi(y, x)
}

function vec_cos(a, b,   a0, b0, ab) {
    ab = vec_dot(a, b)
    a0 = vec_abs(a)
    b0 = vec_abs(b)
    if (a0 == 0) err("[vec_cos] a0 = 0")
    if (b0 == 0) err("[vec_cos] b0 = 0")
    return ab/(a0 * b0)
}

function vec_string(a,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    return sprintf("[%g %g %g]", a[X], a[Y], a[Z])
}

function vec_printf(a, fmt,   X, Y, Z) {
    if (emptyp(fmt)) err_push("[vec_printf] fmt is empty")
    X = 0; Y = 1; Z = 2
    printf fmt, a[X], a[Y], a[Z]
}

function vec_pprintf(pipe, a, fmt,   X, Y, Z) {
    if (emptyp(pipe)) err("[vec_pprintf] pipe is empty")
    if (emptyp(fmt))  err("[vec_pprintf] fmt is empty")
    if (!vec_good(a))  err("[vec_pprintf] a is not a vector")
    X = 0; Y = 1; Z = 2
    printf fmt, a[X], a[Y], a[Z] | pipe
}

function vec_fprintf(file, a, fmt,   X, Y, Z) {
    if (emptyp(file)) err("[vec_fprintf] file is empty")
    if (emptyp(fmt))  err("[vec_fprintf] fmt is empty")
    if (!vec_good(a))  err("[vec_fprintf] a is not a vector")
    X = 0; Y = 1; Z = 2
    printf fmt, a[X], a[Y], a[Z] > file
}

function vec_norm(a, e,   r, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    r = vec_abs(a)
    if (r != 0) {
	e[X] = a[X]/r; e[Y] = a[Y]/r; e[Z] = a[Z]/r
    } else {
	e[X] = e[Y] = e[Z] = 0
    }
}

function vec_project(a, b,   e) { # `a' on `b` (scalar)
    vec_norm(b,  e)
    return vec_dot(a, e)
}

function vec_project_vector(a, b,   p, p0, e) { # `a' on `b'
    if (!vec_good(a)) err("[vec_project_vector] bad vector 'a'")
    if (!vec_good(b)) err("[vec_project_vector] bad vector 'b'")
    vec_norm(b,   e)
    p0 = vec_dot(a, e)
    vec_scalar(e, p0,   p)
}

function vec_reject(a, b,    r,   p) { # `a' on `b'
    if (!vec_good(a)) err("[vec_reject] bad vector 'a'")
    if (!vec_good(b)) err("[vec_reject] bad vector 'b'")

    vec_project_vector(a, b,   p)
    vec_minus(a, p,   r)
}

function vec_copy(a,   b) { copy(a,  b) }

function vec_good(a,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    return (X in a) && (Y in a) && (Z in a)
}

function vec_ini(x, y, z,    a,   X, Y, Z) {
    X = 0; Y = 1; Z = 2
    delete a
    a[X] = x; a[Y] = y; a[Z] = z
}

function vec_arg_ini(    a,    X, Y, Z) {
    X = 0; Y = 1; Z = 2
    delete a
    if (ARGC < 4) err("[vec_arg_ini] not enough args")
    a[X] = ARGV[1]; shift()
    a[Y] = ARGV[1]; shift()
    a[Z] = ARGV[1]; shift()
}
