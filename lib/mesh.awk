# holds [xyz] and t[012]

function mesh_ini(xx, yy, zz, t0, t1, t2,   mesh) {
    _mesh_set_xyz(xx, yy, zz, mesh)
    _mesh_set_tri(t0, t1, t2, mesh)
}

function mesh_ply_ini(file,  mesh,   xx, yy, zz, t0, t1, t2) {
    delete mesh
    if (emptyp(file))
	err("[mesh_ply_ini] file is emptyp")
    ply_read(file,  xx, yy, zz, t0, t1, t2)
    _mesh_set_xyz(xx, yy, zz, mesh)
    _mesh_set_tri(t0, t1, t2, mesh)
}

function mesh_off_ini(file,  mesh,   xx, yy, zz, t0, t1, t2) {
    delete mesh
    if (emptyp(file))
	err("[mesh_off_ini] file is emptyp")
    off_read0(file,  xx, yy, zz, t0, t1, t2)
    _mesh_set_xyz(xx, yy, zz, mesh)
    _mesh_set_tri(t0, t1, t2, mesh)
}

function mesh_xyz(mesh,   xx, yy, zz,   m) {
    delete xx; delete yy; delete zz
    for (m = 0; ("x", m) in mesh; m++) {
	xx[m] = mesh["x", m]
	yy[m] = mesh["y", m]
	zz[m] = mesh["z", m]
    }
}

function mesh_tri(mesh, t0, t1, t2,   m) {
    delete t0; delete t1; delete t2
    for (m = 0; ("t0", m) in mesh; m++) {
	t0[m] = mesh["t0", m]
	t1[m] = mesh["t1", m]
	t2[m] = mesh["t2", m]
    }
}

function mesh_nv(mesh,   m) {
    for (m = 0; ("x", m) in mesh; m++) ;
    return m
}
function mesh_nt(mesh) {
    for (m = 0; ("t0", m) in mesh; m++) ;
    return m
}

function mesh_write_ply(mesh,  file) { err("mesh_write_ply is not implimented") }
function mesh_write_off(mesh,  file,   xx, yy, zz, t0, t1, t2) {
    if (emptyp(file)) err("[mesh_write_off] file is emptyp")
    mesh_xyz(mesh,   xx, yy, zz)
    mesh_tri(mesh,   t0, t1, t2)
    off_write0(file, xx, yy, zz, t0, t1, t2)
}

function mesh_write_gnuplot(mesh,  file,   xx, yy, zz, t0, t1, t2) {
    if (emptyp(file)) err("[mesh_write_off] file is emptyp")
    mesh_xyz(mesh,   xx, yy, zz)
    mesh_tri(mesh,   t0, t1, t2)
    gnuplot_write0(file, xx, yy, zz, t0, t1, t2)
}

function mesh_write_vtk(mesh,  file, xx, yy, zz, t0, t1, t2) {
    if (emptyp(file)) err("[mesh_write_vtk] file is emptyp")
    mesh_xyz(mesh,   xx, yy, zz)
    mesh_tri(mesh,   t0, t1, t2)
    err("mesh_write_vtk is not implimented")
}

function _mesh_set_xyz(xx, yy, zz,    mesh,   m) {
    for (m = 0; m in xx; m++) {
	if (!(m in yy))
	    err(sprintf("[_mesh_set_xyz] no y[%d]", m))
	mesh["x", m] = xx[m]
	mesh["y", m] = yy[m]
	mesh["z", m] = zz[m]
    }
}

function _mesh_set_tri(t0, t1, t2,    mesh,   m) {
    for (m = 0; m in t0; m++) {
	if (!(m in t1))
	    err(sprintf("[_mesh_set_tri] no t1[%d]", m))
	mesh["t0", m] = t0[m]
	mesh["t1", m] = t1[m]
	mesh["t2", m] = t2[m]
    }
}
