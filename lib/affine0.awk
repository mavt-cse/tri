# a wrapper for affine0: uses global variables mesh and affine
function a_ini(Off, Affine) {
    if (emptyp(Off))
	err("[a_ini] Off is empty")
    if (emptyp(Affine))
	err("[a_ini] Affine is empty")

    mesh_off_ini(Off, mesh)
    affine_read(Affine, affine)
}

function a_n() { return affine_n(affine) }

function a_dump(file,    m, x0, y0, z0, x, y, z, color) {
    mesh_xyz(mesh, x0, y0, z0)
    affine_xyz(affine, x0, y0, z0,  x, y, z, color)
    for (m = 0; m in x; m++) {
	vec_get(m, x, y, z,  r)
	vec_fprintf(file, r, "%g %g %g")
	printf " %g\n", color[m]        > file
    }
    close(file)
}

function a_write(file) {
    affine_write(affine, file)
}

function a_copy(from, to,   n) {
    if (!integerp(from))
	err(sprintf("[a_copy] not an integer '%s'", from))
    if (!integerp(to))
	err(sprintf("[a_copy] not an integer '%s'", to))
    n = affine_n(affine)
    if (from >= n || from < 0)
	err(sprintf("[a_copy] no cell number %d", from))
    return affine_copy(affine, from, to)
}

function a_rotx(m, radian) {
    if (!integerp(m))
	err(sprintf("[a_rotx] not an integer '%s'", m))
    if (!numberp(radian))
	err(sprintf("[a_rotx] a numberp '%s'", radian))
    affine_rotx(affine, m, radian)
}

function a_roty(m, radian) {
    if (!integerp(m))
	err(sprintf("[a_roty] not an integer '%s'", m))
    if (!numberp(radian))
	err(sprintf("[a_roty] a numberp '%s'", radian))
    affine_roty(affine, m, radian)
}
function a_rotz(m, radian) {
    if (!integerp(m))
	err(sprintf("[a_rotz] not an integer '%s'", m))
    if (!numberp(radian))
	err(sprintf("[a_rotz] a numberp '%s'", radian))
    affine_rotz(affine, m, radian)
}

function a_dx(m, shift) {
    if (!integerp(m))
	err(sprintf("[a_dx] not an integer '%s'", m))
    if (!numberp(shift))
	err(sprintf("[a_dx] a numberp '%s'", shift))
    affine_shiftx(affine, m, shift)
}
function a_dy(m, shift) {
    if (!integerp(m))
	err(sprintf("[a_dy] not an integer '%s'", m))
    if (!numberp(shift))
	err(sprintf("[a_dy] a numberp '%s'", shift))
    affine_shifty(affine, m, shift)
}
function a_dz(m, shift) {
    if (!integerp(m))
	err(sprintf("[a_dz] not an integer '%s'", m))
    if (!numberp(shift))
	err(sprintf("[a_dz] a numberp '%s'", shift))
    affine_shiftz(affine, m, shift)
}
