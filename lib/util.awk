function msg(s,   p) {
    p = "cat >&2"
    printf "%s: %s\n", prog, s | p
    close(p)
}
function ms0(s,   p) {
   p = "cat >&2"
   printf "%s\n", s | p
   close(p)    
}
function err(s) { msg(s); exit(2) }
function shift(  i) { for (i = 2; i <= ARGC; i++) ARGV[i-1] = ARGV[i]; ARGC-- }
function first_arg(   x) { x = ARGV[1]; shift(); return x }
function noarg() { return ARGC <= 1 }
function eq(a, b) { return a "" == b "" }
function ok(r)    { return r > 0 } # file read status
function bad(r)   { return !ok(r)}
function copy(from, to,   key, val) {
    delete to
    for (key in from) {
	val = from[key]
	to[key] = val
    }
}
function emptyp(x) { return length(x) == 0 }
function arrayp(a,   i) {
    for (i in a) return 1; return 0
}
function integerp(x) { return x ~ /^[0-9][0-9]*$/ }
function numberp(x) { return !emptyp(x) && x + 0 == x }
function quote(s) { return "\"" s "\"" }

function util_nelem(a,   i, c) {
    c = 0
    for (i in a) c++
    return c
}

function queue_push(q, item) {
    if (!("beg" in q)) q["beg"] = q["end"] = 0
    q[0, q["end"]++] = item
}

function queue_pop(q) {
    if (!("beg" in q))
	err("[queue_pop] q is not initialized")
    if (queue_emptyp(q)) err("[queue_pop] q is empty")
    return q[0, q["beg"]++]
}

function queue_emptyp(q) {
    if (!("beg" in q))
	err("[queue_pop] q is not initialized")
    return q["beg"] >= q["end"]
}

# util_member("c", "a b c")?
function util_member(key, s,  a, n) {
    n = split(s, a)
    for (i = 1; i <= n; i++)
	if ("" key == "" a[i]) return 1
    return 0
}
