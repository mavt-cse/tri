function gnuplot_write() {
    if (!arrayp(XX)) err("[gnuplot_write] XX is not an array")
    if (!arrayp(T0)) err("[gnuplot_write] T0 is not an array")
    gnuplot_write0("/dev/stdout", XX, YY, ZZ, T0, T1, T2, FX, FY, FZ)
}

 # f[xyz] are optional
function gnuplot_write0(file, xx, yy, zz, t0, t1, t2, fx, fy, fz) {
    if (emptyp(file)) err("[gnuplot_write0] file is empty")
    if (!arrayp(xx)) err("[gnuplot_write0] xx is not an array")
    if (!arrayp(t2)) err("[gnuplot_write0] t2 is not an array")
    gnuplot_script_set(file)
    gnuplot_data_write(file, xx, yy, zz, t0, t1, t2, fx, fy, fz)
    gnuplot_script_end(file)
    close(file)
}

function gnuplot_data_write(file, xx, yy, zz, t0, t1, t2, fx, fy, fz) {
    if (emptyp(file)) err("[gnuplot_data_write] file is empty")
    if (!arrayp(xx)) err("[gnuplot_data_write] xx is not an array")
    if (!arrayp(t2)) err("[gnuplot_data_write] t2 is not an array")
    if (!arrayp(fx)) {
	gnuplot_script_tri(file)
	gnuplot_data_tri(file, xx, yy, zz, t0, t1, t2)
    } else {
	gnuplot_script_vec(file)
	gnuplot_data_tri(file, xx, yy, zz, t0, t1, t2)
	print "e"
	gnuplot_data_vec(file, xx, yy, zz, fx, fy, fz)
    }
    print "e" > file
}

# local

function gnuplot_script_set(file) {
    print "set hidden3d"                 > file
    print "unset tics"                   > file
    print "unset border"                 > file
    print "unset key"                    > file
    print "set view  60, 30, 2.72, 2.72" > file
    print "set view  equal xyz"          > file
}

function gnuplot_script_tri(file) {
    print  "splot \"-\" w l lw 3 lc 0"   > file
}

function gnuplot_script_vec(file) {
    print  "splot \"-\" w l lw 3 lc 0, \"\" with vectors lw 3" > file
}

function gnuplot_script_end(file) {
    print "pause mouse close" > file
}

function gnuplot_data_tri(file, xx, yy, zz, t0, t1, t2,   m, i, j, k) {
    for (m = 0; m in t0; m++) {
	i = t0[m]; j = t1[m]; k = t2[m]
	if (m != 0)
	    printf "\n\n" > file
	gnuplot_pos_print(file, i, xx, yy, zz)
	gnuplot_pos_print(file, j, xx, yy, zz)
	printf "\n" > file
	gnuplot_pos_print(file, k, xx, yy, zz)
	gnuplot_pos_print(file, k, xx, yy, zz)
    }
}

function gnuplot_data_vec(file, xx, yy, zz, fx, fy, fz, m) {
    for (m = 0; m in xx; m++)
	gnuplot_vec_print(file, m, xx, yy, zz, fx, fy, fz)
}

function gnuplot_pos_print(file, i,  xx, yy, zz,  a) {
    vec_get(i, xx, yy, zz,    a)
    vec_fprintf(file, a, "%g %g %g")
    printf "\n" > file
}

function gnuplot_vec_print(file, i,
			   xx, yy, zz, fx, fy, fz,  a) {
    vec_get(i, xx, yy, zz,    a)
    vec_fprintf(file, a, "%g %g %g")
    vec_get(i, fx, fy, fz,    a)
    vec_fprintf(file, a, " %g %g %g")
    printf "\n" > file
}
