function affine_read(file, mmatrix,   m, i, c, r) {
    if (emptyp(file)) err("[affine_read] file is emptyp")
    # m, c: matrix and coordinate
    for (m = c = 0;;) {
	r = getline < file
	if (bad(r)) break
	for (i = 1; i <= NF; i++) {
	    if (!numberp($i))
		err(sprintf("[affine_read] not a number '%s'" i, $i))
	    mmatrix[m, c++] = $i
	    if (c == 4*4) {c = 0; m++}
	}
    }
    if (c != 0) err(sprintf("[affine_read] wrong file: '%s'", file))
}

function affine_n(mmatrix,   m) {
    for (m = 0; (m, 0) in mmatrix; m++) ;
    return m
}

function affine_write(mmatrix, file,   m, c) {
    if (emptyp(file)) err("[affine_write] file is emptyp")
    for (m = 0; (m, 0) in mmatrix; m++) {
	for (c = 0; (m, c) in mmatrix; ) {
	    printf "%.16g", mmatrix[m, c++]       > file
	    if (c != 0 && c % 4 == 0) printf "\n" > file
	    else printf " "                       > file
	}
    }
}

function affine_get(mmatrix, m,   M, i, j, c) {
    if (!((m, 0) in mmatrix))
	err(sprintf("[affine_get] no m=%d in M", m))
    for (c = i = 0; i < 4; i++)
	for (j = 0; j < 4; j++)
	    M[i, j] = mmatrix[m, c++]
}

function affine_set(mmatrix, m, M,   i, j, c) {
    for (c = i = 0; i < 4; i++)
	for (j = 0; j < 4; j++) {
	    if (!(i, j) in M)
		err(sprintf("[affine_set] no M[%s,%s]", i, j))
	    mmatrix[m, c++] = M[i, j]
	}
}

function affine_split(mmatrix, m,   one, rst,    M, from, to) {
    # mmatrix[m] goes in `one'; the rest -- in `rest'
    delete one; delete rst
    if (!(m, 0) in mmatrix) err(sprintf("no matrix m=%d", m))
    for (from = to = 0; (from, 0) in mmatrix; from++) {
	affine_get(mmatrix, from,   M)
	if (from == m) affine_set(one,    0, M)
	else           affine_set(rst, to++, M)
    }
}

function _affine_extend(a, b) { #
    if (3 in a) err(sprintf("a has a[3]=%s", a[3]))
    copy(a,   b); b[3] = 1
}

function affine_copy(mmatrix, from, to,   M) {
    affine_get(mmatrix, from,    M)
    affine_set(mmatrix,  to,     M)
    return to
}

function affine_xyz(mmatrix, x0, y0, z0,  x, y, z, color,
		    m, M, to, from, r3, r4, r) { # use "template" in
						 # x0, y0, z0 and
						 # populate x, y, z
    delete x; delete y; delete z
    for (to = m = 0; (m, 0) in mmatrix; m++) {
	affine_get(mmatrix, m,    M)
	for (from = 0; from in x0; from++) {
	    vec_get(from, x0, y0, z0,   r3)
	    _affine_extend(r3, r4)
	    matrix_matXvec(M, r4,  r)
	    vec_set(r, to, x, y, z)
	    color[to] = m
	    to++
	}
    }
}

function affine_rotx(mmatrix, m, radian,   M) {
    affine_get(mmatrix, m,   M)
    matrix_rotx(radian,   M)
    affine_set(mmatrix, m, M)
}

function affine_roty(mmatrix, m, radian,   M) {
    affine_get(mmatrix, m,   M)
    matrix_roty(radian,   M)
    affine_set(mmatrix, m, M)
}

function affine_rotz(mmatrix, m, radian,   M) {
    affine_get(mmatrix, m,   M)
    matrix_rotz(radian,   M)
    affine_set(mmatrix, m, M)
}

function affine_shiftx(mmatrix, m, shift,   M) {
    affine_get(mmatrix, m,   M)
    matrix_shiftx(shift,   M)
    affine_set(mmatrix, m, M)
}

function affine_shifty(mmatrix, m, shift,   M) {
    affine_get(mmatrix, m,   M)
    matrix_shifty(shift,   M)
    affine_set(mmatrix, m, M)
}

function affine_shiftz(mmatrix, m, shift,   M) {
    affine_get(mmatrix, m,   M)
    matrix_shiftz(shift,   M)
    affine_set(mmatrix, m, M)
}

function affine_scale(mmatrix, m, scale,   M) {
    affine_get(mmatrix, m,   M)
    matrix_scale(scale,   M)
    affine_set(mmatrix, m, M)
}
