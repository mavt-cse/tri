function transform_rotx(phi, a,  b,    c, s, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    c = cos(phi); s = sin(phi); b[X] = a[X]
    b[Y] = c*a[Y] - s*a[Z]
    b[Z] = s*a[Y] + c*a[Z]
}

function transform_roty(phi, a,  b,    c, s, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    c = cos(phi); s = sin(phi); b[Y] = a[Y]
    b[X] =  c*a[X] + s*a[Z]
    b[Z] = -s*a[X] + c*a[Z]
}

function transform_rotz(phi, a,  b,    c, s, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    c = cos(phi); s = sin(phi); b[Z] = a[Z]
    b[X] = c*a[X] - s*a[Y]
    b[Y] = s*a[X] + c*a[Y]
}
