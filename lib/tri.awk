function tri_normal(a, b, c, e,   n, ab, ac) {
    if (!vec_good(a)) err("[tri_normal] bad vector a")
    vec_minus(b, a,   ab)
    vec_minus(c, a,   ac)
    vec_cross(ab, ac,   n)
    vec_norm(n,   e)
}

function tri_area(a, b, c,   ab, ac, n) {
    if (!vec_good(a)) err("[tri_area] bad vector a")
    vec_minus(b, a, ab)
    vec_minus(c, a, ac)
    vec_cross(ab, ac,   n)
    return vec_abs(n) / 2
}

function tri_angle0(a, b, c,   ba, bc) { # at `b'
    vec_minus(a, b, ba)
    vec_minus(c, b, bc)
    return vec_angle(ba, bc)
}

function tri_ctan0(a, b, c,    ba, bc, x, y) { # at `b'
    y = 2 * tri_area(a, b, c)
    vec_minus(a, b, ba)
    vec_minus(c, b, bc)
    x = vec_dot(ba, bc)
    if (y == 0) {
	err_push("[tri_ctan0] division by zero")
	return 0
    }
    return x/y
}

function tri_angle(a, b, c) {
    RA = tri_angle0(c, a, b)
    RB = tri_angle0(a, b, c)
    RC = tri_angle0(b, c, a)
}

function tri_cos0(a, b, c,   ba, bc, dot) {
    vec_minus(a, b,   ba)
    vec_minus(c, b,   bc)
    return vec_cos(ba, bc)
}

function tri_dih(a, b, c, d,   bc, ang, n, k, nk, x, y) {
    tri_normal(b, c, a,   n)
    tri_normal(c, b, d,   k)
    x = vec_dot(n, k)
    vec_cross(n, k,    nk)
    y = vec_abs(nk)
    ang = atan2(y, x)
    vec_minus(c, b, bc)
    if (vec_dot(bc, nk) < 0)
	ang = - ang
    return ang
}

function tri_edg(a, b, c,   ab, bc, ca) {
    vec_minus(b, a,   ab)
    vec_minus(c, b,   bc)
    vec_minus(a, c,   ca)
}
