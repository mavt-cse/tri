function ply_read(file,   xx, yy, zz, t0, t1, t2,   l, m, OK, EOF, ERR, nv, nt) {
    OK = 1; EOF = 0; ERR = -1
    delete xx; delete yy; delete zz
    delete t0; delete t1; delete t2
    if (emptyp(file))
	err("[ply_read] file is emptyp")
    if (_ply_next(file) != OK)
	err(sprintf("[ply_read] fail to read '%s'", file))
    if (!eq($1, "ply"))
	err(sprintf("[ply_read] '%s' is not a ply file", file))
    _ply_next(file)
    if (!eq($2, "ascii"))
	err(sprintf("[ply_read] '%s' is not ascii", file))

    _ply_next(file)
    nv = $3
    if (!numberp(nv))
	err(sprintf("[ply_read] expecting number of vertices: '%s'", $0))
    do {
	if (_ply_next(file) != OK)
	    err(sprintf("[ply_read] unexpected end of file '%s'", file))
    } while (eq($1, "property"))
    nt = $3
    if (!numberp(nt))
	err(sprintf("[ply_read] expecting number of faces: '%s'", $0))
    do {
	if (_ply_next(file) != OK)
	    err(sprintf("[ply_read] unexpected end of file '%s'", file))
    } while (!eq($1, "end_header"))
    for (m = 0; m < nv; m++) {
	if (_ply_next(file) != OK)
	    err(sprintf("[ply_read] unexpected end of file '%s'", file))
	xx[m] = $1; yy[m] = $2; zz[m] = $3
    }
    for (m = 0; m < nt; m++) {
	if (_ply_next(file) != OK)
	    err(sprintf("[ply_read] unexpected end of file '%s'", file))
	t0[m] = $2; t1[m] = $3; t2[m] = $4
    }
    close(file)
}

function ply_write(file, xx, yy, zz, t0, t1, t2,   ne, nvp, nv, nt, m) {
    if (emptyp(file)) err("[ply_write0] file is emptyp")
    ne = 0; nvp = 3
    nv = util_nelem(xx)
    nt = util_nelem(t0)
    printf "OFF\n"                   > file
    printf "%d %d %d\n", nv, nt, ne  > file
    for (m = 0; (m in xx); m++)
	printf "%.14e %.14e %.14e\n", xx[m], yy[m], zz[m] > file
    for (m = 0; (m in t0); m++)
	printf "%d %d %d %d\n", nvp, t0[m], t1[m], t2[m]  > file
}

function _ply_next(file,  OK, EOF, ERR, s, l) {
    OK = 1; EOF = 0; ERR = -1
    do {
	s = getline < file
	if      (s == ERR) return ERR
	else if (s == EOF) return EOF
    } while (_ply_comment($0))
    return OK
}
function _ply_comment(l) { return l ~ /^comment/ }
