# set: set[v0], set[v1], .. vertices to keep in output mesh ('omesh')
# radius: keep is distance from one of v0, v1, .. <= radius
function select0(imesh, set, radius,   omesh,   t0, t1, t2, v, adj, distance) {
    if (!integerp(radius))
	err(sprintf("radius='%s' is not an integer", radius))
    if (radius <= 0)
	err(sprintf("radius=%s <= 0", radius))

    mesh_tri(imesh,   t0, t1, t2)
    adj_ini(t0, t1, t2,   adj)

    for (v in set) {
	if (!(v, 0) in adj)
	    err(sprintf("[select0] '%s' is in set but not in `imesh`", v))
	adj_distance(adj, v, radius,   distance)
    }
    _select_build_mesh(set, distance, imesh,    omesh)
}

function _select_build_mesh(set, keep, imesh,   omesh,  idx,
			    #
			    ix,  iy,  iz,    ox,  oy,  oz,
			    it0, it1, it2,   ot0, ot1, ot2) {
    mesh_tri(imesh,   it0, it1, it2)
    mesh_xyz(imesh,    ix, iy, iz)

    _select_build_index(set, keep,   idx)

    _select_tri(idx, it0, it1, it2,   ot0, ot1, ot2)
    _select_array(idx, ix,   ox)
    _select_array(idx, iy,   oy)
    _select_array(idx, iz,   oz)
    mesh_ini(ox, oy, oz, ot0, ot1, ot2,   omesh)
}

# idx: map from old to new
function _select_tri(idx, it0, it1, it2,  ot0, ot1, ot2,
		     #
		     m, i, j, k, to) {
    delete ot0; delete ot1; delete ot2; _INDEX = 0
    for (m = to = 0; m in it0; m++) {
	i = it0[m]; j = it1[m]; k = it2[m]
	if (!(i in idx)) continue
	if (!(j in idx)) continue
	if (!(k in idx)) continue
	ot0[to] = idx[i]
	ot1[to] = idx[j]
	ot2[to] = idx[k]
	to++
    }
}

function _select_array(idx, A,   B,   from, to) {
    delete B
    for (from in idx) {
	to = idx[from]
	B[to] = A[from]
    }
}

# put A first
function _select_build_index(A, B,   idx,   from, to) {
    delete idx
    to = 0
    for (from in A)
	idx[from] = to++
    for (from in B) {
	if (from in A) continue
	idx[from] = to++
    }
}
