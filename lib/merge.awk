# merge triplets of numbers

function _hash_reg(i, j, k,   x, y, z, n) {
    n = _nhash++
    for (x = i - 1; x <= i + 1; x++)
	for (y = j - 1; y <= j + 1; y++)
	    for (z = k - 1; z <= k + 1; z++)
		_hash[x, y, z] = n
    return _hash[i, j, k]
}

function _hash_key(i, j, k) {
    if ((i, j, k) in _hash) return _hash[i, j, k]
    else return -1
}

function _merge_key(x, y, z,   xx, yy, zz,    c, i, j,k, n) {
    c = 1e-6 # TODO: cutoff
    i = int(x/c); j = int(y/c); k = int(z/c)
    n = _hash_key(i, j, k)
    if (n == -1) {
	n = _hash_reg(i, j, k)
	xx[n] = x; yy[n] = y; zz[n] = z
    }
    return n
}

function merge(x0, y0, z0, x1, y1, z1, x2, y2, z2,
	       xx, yy, zz, t0, t1, t2,   i, j, h0, h1, h2, tri) {
    _nhash = 0
    delete _hash

    for (i = j = 0; i in x0; i++) {
	h0 = _merge_key(x0[i], y0[i], z0[i], xx, yy, zz)
	h1 = _merge_key(x1[i], y1[i], z1[i], xx, yy, zz)
	h2 = _merge_key(x2[i], y2[i], z2[i], xx, yy, zz)

	if (!(h0, h1, h2) in tri) {
	    t0[j] = h0
	    t1[j] = h1
	    t2[j] = h2
	    j++

	    tri[h0, h1, h2]
	    tri[h1, h2, h0]
	    tri[h2, h0, h1]
	}
    }
    return _nhash
}

function merge_off0(off, x0, y0, z0, x1, y1, z1, x2, y2, z2,
		   xx, yy, zz, t0, t1, t2, n) {
    n = merge(x0, y0, z0,
	      x1, y1, z1,
	      x2, y2, z2,
	      xx, yy, zz, t0, t1, t2)
    off_write0(off, xx, yy, zz, t0, t1, t2)
    return n
}

function merge_off(x0, y0, z0, x1, y1, z1, x2, y2, z2,
		   xx, yy, zz, t0, t1, t2, n) {
    n = merge(x0, y0, z0,
	      x1, y1, z1,
	      x2, y2, z2,
	      xx, yy, zz, t0, t1, t2)
    off_write0("/dev/stdout", xx, yy, zz, t0, t1, t2)
    return n
}
