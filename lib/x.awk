function  nxt(h) { return NEXT_HDG[h] }
function  flp(h) { return FLIP_HDG[h] }
function  ver(h) { return VER_HDG[h] }
function h_bnd(h) { return h in HDG_BND }
function v_bnd(v) { return v in VER_BND }
function get_edg(i, j,   r,    a, b) {
    vec_get(i, XX, YY, ZZ, a)
    vec_get(j, XX, YY, ZZ, b)
    vec_minus(a, b, r)
}
function get3(i, j, k,     a, b, c) {
    vec_get(i, XX, YY, ZZ, a); vec_get(j, XX, YY, ZZ, b); vec_get(k, XX, YY, ZZ, c)
}
