function vtk_push(key, data, vert,   i, has) {
    for (i = 0; i in data; i++) {
	vert[key, i] = data[i]
	has = 1
    }
    if (!has) err("[vtk_push] empty 'data'")
}

function vtk_write(vert, tri,   keys, key, nv, nt) {
    nv = util_nelem(XX)
    nt = util_nelem(T0)
    vtk_header()
    vtk_points(nv)
    vtk_tri(nt)
    if (vtk_keys(vert, keys)) {
	vtk_vert_header(nv)
	for (key in keys)
	    vtk_data(key, vert)
    }
    if (vtk_keys(tri, keys)) {
	vtk_tri_header(nt)
	for (key in keys)
	    vtk_data(key, tri)
    }
}

function vtk_keys(vert, keys,   k, a, fst, has) {
    delete keys
    has = 0
    for (k in vert) {
	split(k, a, SUBSEP)
	fst = a[1]
	keys[fst]
	has = 1
    }
    return has
}

function vtk_header() {
    printf "# vtk DataFile Version 2.0\n"
    printf "created with uDeviceX\n"
    printf "ASCII\n"
    printf "DATASET POLYDATA\n"
}

function vtk_points(nv,   m) {
    printf "POINTS %d double\n", nv
    for (m = 0; m in XX; m++) {
	if (!(m in XX)) err("[vtk_points] XX does not have " m)
	printf "%.14e %.14e %.14e\n", XX[m], YY[m], ZZ[m]
    }
}

function vtk_tri(nt,   nvp, m) {
    nvp = 3
    nt = util_nelem(T0)
    printf "POLYGONS %d %d\n", nt, (1 + nvp)*nt
    for (m = 0; m < nt; m++)
	printf "%d %d %d %d\n", nvp, T0[m], T1[m], T2[m]
}

function vtk_vert_header(nv) {
    printf "POINT_DATA %d\n", nv
}
function vtk_tri_header(nt) {
    printf "CELL_DATA %d\n", nt
}
function vtk_data(key, data,   m) {
    printf "SCALARS %s double 1\n", key
    printf "LOOKUP_TABLE default\n"
    for (m = 0; (key, m) in data; m++)
	printf "%.14e\n", data[key, m]
}
