function err_ok() { return !ERR }
function err_push(m, t,  i) {
    for (i = 0; i in ERR_MSG; i++) ;
    ERR_MSG[i] = m
    ERR = 1
}
function err_abort(m,   i) {
    for (i = 0; i in ERR_MSG; i++)
	msg(sprintf("%s", ERR_MSG[i]))
    if (!emptyp(m)) err(m)
}
