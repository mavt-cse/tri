function off_read0(file,   xx, yy, zz, t0, t1, t2,   nv, nt, l, r, i) {
    delete xx; delete yy; delete zz
    delete t0; delete t1; delete t2
    if (emptyp(file)) err("[off_read0] file is emptyp")
    r = getline < file
    if (bad(r)) err("[off_read0] fail to read file: " file)
    if (!eq($0, "OFF")) err(sprintf("[off_read0] not an OFF file '%s'", file))
    for (;;) { # comments
	r = getline < file
	if (bad(r)) err("fail to read file: " file)
	if (!off_comment($0)) break
    }
    nv = $1; nt = $2
    for (i = 0; i < nv; i++) {
	r = getline < file
	if (bad(r)) err("fail to read verts: " file)
	xx[i] = $1; yy[i] = $2; zz[i] = $3
    }
    for (i = 0; i < nt; i++) {
	r = getline < file
	if (bad(r)) err("fail to read tris: " file)
	t0[i] = $2; t1[i] = $3; t2[i] = $4
    }
    close(file)
}

function off_read(file) {  # sets NV, NT, [XX, YY, ZZ], [T0, T1, T2]
    if (emptyp(file)) err("[off_read] file is emptyp")
    off_read0(file, XX, YY, ZZ, T0, T1, T2)
    NV = util_nelem(XX)
    NT = util_nelem(T0)
}

function off_write0(file, xx, yy, zz, t0, t1, t2,   ne, nvp, nv, nt, m) {
    if (emptyp(file)) err("[off_write0] file is emptyp")
    ne = 0; nvp = 3
    nv = util_nelem(xx)
    nt = util_nelem(t0)
    printf "OFF\n"                   > file
    printf "%d %d %d\n", nv, nt, ne  > file
    for (m = 0; (m in xx); m++)
	printf "%.14e %.14e %.14e\n", xx[m], yy[m], zz[m] > file
    for (m = 0; (m in t0); m++)
	printf "%d %d %d %d\n", nvp, t0[m], t1[m], t2[m]  > file
}

function off_write() { off_write0("/dev/stdout", XX, YY, ZZ, T0, T1, T2) }
function off_comment(s) { return s ~ /^[ \t]*#/ }
