# build adjacency structure from triangles
function adj_ini(t0, t1, t2,   adj,   m, i, j, k) {
    for (m in t0) {
	i = t0[m]; j = t1[m]; k = t2[m]
	_adj_connect(adj, i, j)
	_adj_connect(adj, j, k)
	_adj_connect(adj, k, i)
    }
}

function adj_write(file, adj,   v, r) {
    for (v = 0; (v, 0) in adj; v++) {
	printf v >  file
	for (r = 0; (v, r) in adj; r++)
	    printf " %d", adj[v, r] > file
	printf "\n"                > file
    }
}

function adj_distance(adj, i, radius,   distance,   q, r, j) {
    distance[i] = 0
    queue_push(q, i)
    while (!(queue_emptyp(q))) {
	i = queue_pop(q)
	if ((d = distance[i]) == radius) continue
	for (r = 0; (i, r) in adj; r++) {
	    if ((j = adj[i, r]) in distance) continue
	    distance[j] = d + 1
	    queue_push(q, j)
	}
    }
}

function _adj_connect(adj, i, j) {
    _adj_connect0(adj, i, j)
    _adj_connect0(adj, j, i)
}

function _adj_connect0(adj, i, j,   r) {
    for (r = 0; (i, r) in adj; r++)
	if (adj[i, r] == j) return
    adj[i, r] = j
}
