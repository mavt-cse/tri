function setup_flip(i, j) {
    h = _HDG[i, j]
    if ((j, i) in _HDG)
	FLIP_HDG[h] = _HDG[j, i]
}

function setup_edg(i, j,   e, h) {
    if ((j, i) in _EDG) {
	e = _EDG[j, i]
    } else {
	e = _EDG[i, j] = NE++	
	E0[e] = i; E1[e] = j
    }
    h = _HDG[i, j]
    HDG_EDG[e] = h
    EDG_HDG[h] = e
}

function setup_dih(i, j,   d, h) {
    if ((j, i) in _DIH) return
    h = _HDG[i, j]
    if (!(h in FLIP_HDG)) return
    d = ND
    D1[d] = i; D2[d] = j
    D0[d] = VER_HDG[NEXT_HDG[NEXT_HDG[h]]]
    D3[d] = VER_HDG[NEXT_HDG[NEXT_HDG[FLIP_HDG[h]]]]
    _DIH[i, j] = d
    ND++
}

function setup_tri(h, t, i, j) {
    HDG_TRI[t] = HDG_VER[i] = _HDG[i, j] = h
    VER_HDG[h] = i; TRI_HDG[h] = t
}
function setup_hdg(   t, i, j, k, h, f, n, nn, hi, hj, hk, h0, r) {
    for (h = t = 0; t in T0; t++) {
	i = T0[t]; j = T1[t]; k = T2[t]
	setup_tri(hi = h++, t, i, j)
	setup_tri(hj = h++, t, j, k)
	setup_tri(hk = h++, t, k, i)
	NEXT_HDG[hi] = hj; NEXT_HDG[hj] = hk; NEXT_HDG[hk] = hi
    }
    NH = h

    for (t = 0; t in T0; t++) {
	i = T0[t]; j = T1[t]; k = T2[t]
	setup_flip(i, j); setup_flip(j, k); setup_flip(k, i)
    }

    delete _EDG; NE = 0
    for (t = 0; t in T0; t++) {
	i = T0[t]; j = T1[t]; k = T2[t]
	setup_edg(i, j); setup_edg(j, k); setup_edg(k, i)
    }

    delete _DIH; ND = 0
    for (t = 0; t in T0; t++) {
	i = T0[t]; j = T1[t]; k = T2[t]
	setup_dih(i, j); setup_dih(j, k); setup_dih(k, i)
    }

    for (h = 0; h in NEXT_HDG; h++) {
	if (h in FLIP_HDG) continue
	i = VER_HDG[h]
	VER_BND[i] = 1
    }

    for (v = 0; v < NV; v++) {
	if (v in VER_BND) continue
	h0 = h = HDG_VER[v]; r = 0
	do {
	    n = NEXT_HDG[h]; nn = NEXT_HDG[n]
	    R0[v,r] = v
	    R1[v,r] = VER_HDG[n]
	    R2[v,r] = VER_HDG[nn]
	    r++
	    h = FLIP_HDG[nn]
	} while (h != h0)
    }
}

function setup() {
    delete _HDG
    delete E0; delete E1
    delete D0; delete D1; delete D2; delete D3
    delete R0; delete R1; delete R2
    delete VER_BND

    setup_hdg()
}
