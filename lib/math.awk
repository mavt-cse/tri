function copy_sign(a, b) { return b >= 0 ? abs(a) : -abs(a) }

function asin(x) {
    if (abs(x) > 1) err(sprintf("error asin(%s)", x))
    return atan2(x, sqrt(1 - x*x))
}

function acos(x) {
    if (abs(x) > 1) err(sprintf("error acos(%s)", x))
    return atan2(sqrt(1-x*x), x)
}
function ctan(x) { return cos(x)/sin(x) }

function pow(a, b) {return a^b }
function abs(a) { return (a > 0) ? a : -a }
function atan2_pi(y, x) { return abs(atan2(y, x)) }

function deg2rad(x,   pi) {
    pi = 3.141592653589793
    return x*pi/180
}

function rad2deg(x,   pi) {
    pi = 3.141592653589793
    return x*180/pi
}

function max(a, b) { return a > b ? a : b }
function min(a, b) { return a < b ? a : b }
function math_abs(x)    { return x >= 0 ? x : -x }
function math_sgn(x)    { return x >= 0 ? 1 : -1 }
