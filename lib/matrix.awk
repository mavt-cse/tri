function matrix_ini3x3(a, b, c,
		       d, e, f,
		       g, h, l,   M,   i, j) {
    i = 0
    j = 0; M[i,j++] = a; M[i,j++] = b; M[i,j++] = c; i++
    j = 0; M[i,j++] = d; M[i,j++] = e; M[i,j++] = f; i++
    j = 0; M[i,j++] = g; M[i,j++] = h; M[i,j++] = l
}

function matrix_print(M,   i, j) {
    if (!(0, 0) in M) err("[matrix_print] M is not a matrix")
    for (i = 0; (i, 0) in M; i++) {
	if (i != 0) printf "\n"
	for (j = 0; (i, j) in M; j++) {
	    if (j != 0) printf " "
	    printf("%g", M[i, j])
	}
    }
    printf "\n"
}

function matrix_matXvec(M, a,  b,   i, j) {
    if (!(0, 0) in M) err("[matrix_matXvec] M is not a matrix")
    if (!(0 in a))    err("[matrix_matXvec] a is not a vector")
    delete b
    for (i = 0; (i, 0) in M; i++)
	for (j = 0; (i, j) in M; j++) {
	    if (!(j in a))
		err(sprintf("[matrix_matXvec] have M[%s,%s] but no a[%s]", i, j, j))
	    b[i] += M[i,j] * a[j]
	}
}

function matrix_matXmat(M, N,   L,   i, j) {
    if (!(0, 0) in M) err("[matrix_matXmat] M is not a matrix")
    if (!(0, 0) in N) err("[matrix_matXmat] N is not a matrix")
    delete L
    for (i = 0; (i, 0) in M; i++)
	for (j = 0; (i, j) in M; j++) {
	    if (!(i, j) in N)
		err(sprintf("[matrix_matXmat] have M[%s,%s] but no N[%s,%s]", i, j, i, j))
	    L[i,j] += M[i,j] * N[j,i]
	}
}

function _matrix_rot(radian, d0, d1, M,    N,   X, Y, Z, s, c, i) {
    if (!(0, 0) in M) err("[_matrix_rot] M is not a matrix")
    X = 0; Y = 1; Z = 2
    s = sin(radian); c = cos(radian)
    for (i = 0; i < 3; i++) {
	N[i,d0] = c*M[i,d0] - s*M[i,d1]
	N[i,d1] = s*M[i,d0] + c*M[i,d1]
    }
}

function matrix_rotx(radian, M,        N, X, Y, Z) { # rotate in-place
    X = 0; Y = 1; Z = 2
    copy(M,   N)
    _matrix_rot(radian, Y, Z, N,   M)
}

function matrix_roty(radian, M,        N, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    copy(M,   N)
    _matrix_rot(radian, Z, X, N,   M)
}

function matrix_rotz(radian, M,        N, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    copy(M,   N)
    _matrix_rot(radian, X, Y, N,   M)
}

function matrix_shiftx(shift, M,    X) {
    X = 0
    M[X,3] += shift
}

function matrix_shifty(shift, M,    X, Y) {
    X = 0; Y = 1
    M[Y,3] += shift
}

function matrix_shiftz(shift, M,    X, Y, Z) {
    X = 0; Y = 1; Z = 2
    M[Z,3] += shift
}

function matrix_scale(scale, M,   i, j) {
    if (!(0, 0) in M)
	err("[matrix_scale] M is not a matrix")
    if (scale == 0)
	err("[matrix_scale] scale=0")
    for (i = 0; i < 3; i++)
	for (j = 0; j < 3; j++)
	    M[i,j] *= scale
}
