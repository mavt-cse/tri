function dtri_area0(a, b, c,  da,   n, n2, bc) { # for `a'
    tri_normal(a, b, c,    n)
    vec_scalar(n, 1/2,   n2)
    vec_minus(c, b,   bc)
    vec_cross(n2, bc,   da)
}

function dtri_area(a, b, c,  da, db, dc,   n, n2, ab, bc, ca) {
    if (!vec_good(a)) {
	err_push("dtri_area: bad vector a")
	return
    }
    tri_normal(a, b, c,   n)
    if (!err_ok()) {
	err_push("dtri_area")
	return
    }
    vec_scalar(n, 1/2,   n2)
    tri_edg(a, b, c,   ab, bc, ca)

    vec_cross(n2, ab,   dc)
    vec_cross(n2, bc,   da)
    vec_cross(n2, ca,   db)
}

function dtri_edg(a, b,  da, db,   ab, e) {
    vec_minus(b, a,   ab)
    vec_norm(ab,   e)

    vec_copy(e,     db)
    vec_negative(e, da)
}

function dtri_edg2(a, b,  da, db,   ab) { # edg squred
    vec_minus(b, a,   ab)
    vec_scalar(ab, 2,  da)
    vec_negative(da,   db)
}

function _angle1(a, b, n,   da,   d2, ab, nab) {
    # compute: n x (b - a)/|b - a|^2
    vec_minus(b, a,  ab)
    vec_cross(n, ab, nab)
    d2 = vec_dot(ab, ab)
    vec_scalar(nab, 1/d2,  da)
}

function dtri_angle0(a, b, c,   da, db, dc,   ab, ac, n, nda) {
    # derivative of interior angle at `b'
    tri_normal(b, c, a,   n)
    _angle1(c, b, n,   dc)
    _angle1(b, a, n,   da)
    vec_plus(dc, da,  nda)
    vec_negative(nda,    db)
}


function dtri_ctan0(a, b, c,   da, db, dc,   da0, db0, dc0, s, ang) {
    ang = tri_angle0(a, b, c)
    if (ang == 0) err_push("[dtri_ctan0] ang = 0")
    s = -1/sin(ang)^2
    dtri_angle0(a, b, c,   da0, db0, dc0)
    vec_scalar(da0, s,   da)
    vec_scalar(db0, s,   db)
    vec_scalar(dc0, s,   dc)
}

# p = angle(u, v). return diff(p, u)
function _dtri_cos(u, v,   du,   u0, v0, du0) {
    u0 = vec_abs(u)
    v0 = vec_abs(v)
    vec_reject(v, u,  du0)
    vec_scalar(du0,  1/(u0*v0),   du)
}

function dtri_cos(a, b, c,   da, db, dc,   u, v, du, dv) { # at `b`
    vec_minus(a, b,  u)
    vec_minus(c, b,  v)

    _dtri_cos(u, v,   du)
    _dtri_cos(v, u,   dv)

    vec_linear_combination( 1, du,   0, dv,   da)
    vec_linear_combination(-1, du,  -1, dv,   db)
    vec_linear_combination( 0, du,   1, dv,   dc)
}

function dtri_cos_a(a, b, c,   da,   u, v, du, dv) {
    vec_minus(a, b,  u)
    vec_minus(c, b,  v)
    _dtri_cos(u, v,   da)
}

function dtri_cos_b(a, b, c,   db,   u, v, du, dv) {
    vec_minus(a, b,  u)
    vec_minus(c, b,  v)
    _dtri_cos(u, v,   du)
    _dtri_cos(v, u,   dv)
    vec_linear_combination(-1, du,  -1, dv,   db)
}

function dtri_dih(a, b, c, d,   da, db, dc, dd,
		  n, k, bc, e, Ak, An,
		  cn, bn, ck, bk) {
    tri_normal(a, b, c,   n)
    tri_normal(c, b, d,   k)

    vec_minus(c, b,   bc)
    e = vec_abs(bc)

    An = tri_area(a, b, c)
    Ak = tri_area(c, b, d)

    vec_scalar(n, e/(2*An),   da)
    vec_scalar(k, e/(2*Ak),   dd)

    cn = tri_ctan0(b, c, a)
    bn = tri_ctan0(a, b, c)

    bk = tri_ctan0(c, b, d)
    ck = tri_ctan0(d, c, b)

    vec_linear_combination(-bn/e, n, -bk/e, k,    dc)
    vec_linear_combination(-cn/e, n, -ck/e, k,    db)
}
