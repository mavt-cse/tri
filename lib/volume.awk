function volume_tri(a, b, c,   n) {
    # oriented volume of tetrahedral [0, a, b, c]
    vec_cross(a, b,   n)
    return vec_dot(c, n)/6
}

function volume_side(a, b, c, d) {
    # oriented volume of a piramide [0, a, b, c, d]
    return volume_tri(a, b, c) + volume_tri(c, d, a)
}

function volume_prism(a0, b0, c0,   a1, b1, c1,   V) {
    # volume between two triangles
    V += volume_side(a0, a1,   b1, b0)
    V += volume_side(b0, b1,   c1, c0)
    V += volume_side(c0, c1,   a1, a0)

    V += volume_tri(a0, b0, c0)
    V -= volume_tri(a1, b1, c1)
    return V
}
