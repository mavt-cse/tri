#!/bin/sh
#############################################################
#We calculate the bending energy and force using Meyer et al
#model and its associated quantities
#
#We take the same convention as in Meyer et al 2003,
#where sphere has a positive mean curvature.
#
#Reference: Meyer, Desbrun, Schroeder, and Barr, in
#           <<Visualization and Mathematics III>> 2003
#           Guckenberger, Schrami, Chen, Leonetti, and Gekle,
#           Comput. Phys. Comm. 207, 1-23, 2016
#############################################################
tri.awk util off setup tri math vec -e '
BEGIN {
    prog = "Meyer et al"
    if (noarg()) err("needs OFF file")
    file_in = ARGV[1]; shift()
    file_out1=ARGV[1]; shift()
    file_out2=ARGV[1]; shift()
    file_out3=ARGV[1]; shift()
    fout3_new=ARGV[1];
   
    #print file_in, file_out1, file_out2, file_out3, fout3_new

    off_read(file_in)
    setup()

    #**********************************************
    #1st loop
    #This loops over all edges and
    #compute the Laplace-Beltrami operator applied on positions.
    #Meanwhile, it computes the Voroni area for each vertex.
    #**********************************************

    X = 0; Y = 1; Z = 2

    for (m = 0; m < NE; m++) {

        if (m in EDG_BND) continue

	i = D0[m]; j = D1[m]; k = D2[m]; l = D3[m]

	a[X] = XX[i]; a[Y] = YY[i]; a[Z] = ZZ[i]
	b[X] = XX[j]; b[Y] = YY[j]; b[Z] = ZZ[j]
	c[X] = XX[k]; c[Y] = YY[k]; c[Z] = ZZ[k]
	d[X] = XX[l]; d[Y] = YY[l]; d[Z] = ZZ[l]

	cot1 = tri_ctan0(c,a,b)
	cot2 = tri_ctan0(b,d,c)
	Tjk  = cot1 + cot2

	LBX[j] += Tjk*(b[X]-c[X])/2
	LBY[j] += Tjk*(b[Y]-c[Y])/2
	LBZ[j] += Tjk*(b[Z]-c[Z])/2
	
	LBX[k] -= Tjk*(b[X]-c[X])/2
	LBY[k] -= Tjk*(b[Y]-c[Y])/2
	LBZ[k] -= Tjk*(b[Z]-c[Z])/2
	
	len2 = (b[X]-c[X])^2+(b[Y]-c[Y])^2+(b[Z]-c[Z])^2

	#for the moment we do not use the mixed area,
	#but the Voronoi area.
	#The two areas are equivalent for non-obtuse triangles
	A[j] += Tjk*len2/8
	A[k] += Tjk*len2/8
	
    }

    #**********************************************
    #2nd loop
    #This loops over all triangles and
    #compute the Guassian curvature and
    #normal vector for each vertex
    #compute total triangle area
    #**********************************************

    area_tot_tri = 0 
    X = 0; Y = 1; Z = 2

    for (m = 0; m < NT; m++ ) {

	i = T0[m]; j = T1[m]; k = T2[m]

	a[X] = XX[i]; a[Y] = YY[i]; a[Z] = ZZ[i]
	b[X] = XX[j]; b[Y] = YY[j]; b[Z] = ZZ[j]
	c[X] = XX[k]; c[Y] = YY[k]; c[Z] = ZZ[k]

	tri_angle(a, b, c)

        curva_gauss[i] -= RA
        curva_gauss[j] -= RB
        curva_gauss[k] -= RC

	#print "RA, RB, RC", RA, RB, RC

	tri_normal(a, b, c,   n)

	#we note the difference in normal convention
	#the tri_normal() routine uses right-hand rule
        #while Guckenberge et al 2016 use sphere curvature
        #as positive. They differ.
	NORMX[i] += RA*n[X]
	NORMY[i] += RA*n[Y]
	NORMZ[i] += RA*n[Z]

	NORMX[j] += RB*n[X]
	NORMY[j] += RB*n[Y]
	NORMZ[j] += RB*n[Z]

	NORMX[k] += RC*n[X]
	NORMY[k] += RC*n[Y]
	NORMZ[k] += RC*n[Z]

	area  = tri_area(a, b, c)
	area_tot_tri += area

    }
    #**********************************************
    #3rd loop
    #This loops over all vertices and 
    #1)normalize the Gaussian curvature and
    #2)normal vector and
    #3)Laplace-Beltrami operator 
    #for each vertex.
    #Afterwards,
    #1)Calculate mean curvature
    #2)Calculate the energy, total energy and total area
    #3)Calculate first part of the force
    #**********************************************

    pi = 3.141592653589793

    eng_tot = 0
    area_tot_voro = 0

    for (m = 0; m < NV; m++) {
    
	curva_gauss[m] = ( curva_gauss[m] + 2 * pi ) / A[m]
	
	len = NORMX[m]^2 + NORMY[m]^2 + NORMZ[m]^2
	len = sqrt(len)
	NORMX[m] /= len
	NORMY[m] /= len
	NORMZ[m] /= len
	
	LBX[m] /= A[m]
	LBY[m] /= A[m]
	LBZ[m] /= A[m]

	len = LBX[m]^2 + LBY[m]^2 + LBZ[m]^2
	len = sqrt(len)
	LBNORMX[m]=LBX[m]/len
	LBNORMY[m]=LBY[m]/len
	LBNORMZ[m]=LBZ[m]/len

	#print LBNORMX[m], LBNORMY[m], LBNORMZ[m], NORMX[m], NORMY[m], NORMZ[m]
	
	curva_mean[m]  = LBX[m]*NORMX[m] + LBY[m]*NORMY[m] + LBZ[m]*NORMZ[m]
	curva_mean[m] /= 2

	#curva_mean_self[m]  = LBX[m]*LBNORMX[m] + LBY[m]*LBNORMY[m] + LBZ[m]*LBNORMZ[m]
	#curva_mean_self[m] /= 2

	ENERGY_DEN[m] = 2*curva_mean[m]^2 
	ENERGY[m]     = ENERGY_DEN[m]*A[m]

	eng_tot       += ENERGY[m]
	area_tot_voro += A[m]

	FORCE[m]=2*2*curva_mean[m]*(curva_mean[m]^2-curva_gauss[m])
 
    }
 
    #**********************************************
    #4th loop
    #This loop over all edges and
    #compute the Laplace-Beltrami operator applied on mean curvature
    #and also add it to the total force on each vertex
    #**********************************************

    X = 0; Y = 1; Z = 2

    for (m = 0; m < NE; m++) {

        if (m in EDG_BND) continue

	i = D0[m]; j = D1[m]; k = D2[m]; l = D3[m]

	a[X] = XX[i]; a[Y] = YY[i]; a[Z] = ZZ[i]
	b[X] = XX[j]; b[Y] = YY[j]; b[Z] = ZZ[j]
	c[X] = XX[k]; c[Y] = YY[k]; c[Z] = ZZ[k]
	d[X] = XX[l]; d[Y] = YY[l]; d[Z] = ZZ[l]

	cot1 = tri_ctan0(c,a,b)
	cot2 = tri_ctan0(b,d,c)
	Tjk  = cot1 + cot2

	LBH[j] += Tjk*(curva_mean[j]-curva_mean[k])/2/A[j]
	LBH[k] -= Tjk*(curva_mean[j]-curva_mean[k])/2/A[k]
	
	FORCE[j] += Tjk*(curva_mean[j]-curva_mean[k])/A[j]
	FORCE[k] -= Tjk*(curva_mean[j]-curva_mean[k])/A[k]

    }


    #**********************************************
    #The last loop
    #This loops over all vertices and
    #print associated quantities of each vertex  
    #**********************************************

    print "#1theta:2axis dist:3x:4y:5z:6mean curvature:7gaussian curvature:8energy:9energy density:10laplace-beltrami of H:11voroni area" > file_out1
    print "#1theta:2axis dist:3x:4y:5z:6force_density:7force:8force_density_x:9force_density_y:10force_density_z" > file_out2

    for (m = 0; m < NV; m++) {

	x = XX[m]; y = YY[m]; z = ZZ[m]
	r = sqrt(x^2 + y^2)
	rr= sqrt(x^2 + y^2 + z^2)

	theta = acos(z/rr)
	if (theta > pi/2 ) {
	  theta = pi - theta
	}	

	print theta, r, XX[m], YY[m], ZZ[m], curva_mean[m], curva_gauss[m], ENERGY[m], ENERGY_DEN[m], LBH[m], A[m] >> file_out1
	print theta, r, XX[m], YY[m], ZZ[m], FORCE[m], FORCE[m]*A[m], FORCE[m] * NORMX[m], FORCE[m] * NORMY[m], FORCE[m] * NORMZ[m] >> file_out2

    }
    
    print "#total energy:", eng_tot >> file_out1
    print "#total triangle area:", area_tot_tri >> file_out1
    print "#total voronoi area :", area_tot_voro >> file_out1

    if ( fout3_new ) {

        print "#number of triangles, total energy, total triangle area, total voronoi area" > file_out3
 
    }
    print NT, eng_tot, area_tot_tri, area_tot_voro >> file_out3

}' -- "$@"
