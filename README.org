* Introduction

A library for the modelling of surfaces shaped by various forces and
constraints.

* Installation

A library consists of several executable and a collection of awk
functions. The executables have prefixes =tri.=, for example
=tri.awk=.

Command
#+BEGIN_SRC sh
make install PREFIX=$HOME/prefix
#+END_SRC
installs excutables to =$HOME/prefix/bin=. This directory should be on
the =PATH=.

* Testing

#+BEGIN_SRC sh
make test
#+END_SRC

runs tests (requires atest)

* How to use tri?

A [[file:example/hellow/main][file]] is shell script which uses three libraries: =err=, =util=,
=vec=. A string after =-e= is an awk program which calls functions
from the libraries.
#+BEGIN_SRC sh
#!/bin/sh

tri.awk err util vec -e '
BEGIN {
   vec_ini(1, 2, 3,    a)
   vec_ini(10, 20, 30, b)
   vec_plus(a, b,   c)
   msg(vec_string(c))
}' -- "$@"
#+END_SRC

Run this script
#+BEGIN_SRC sh
$ example/hellow/main
: [11 22 33]
#+END_SRC

* off_read()
NV, NT: number of verts, triangles
XX[v] YY[v] ZZ[v]: position
T0[t] T1[t] T2[t]: vertices in triangle `t'

* setup()

NE: number of edges
D0[d] {D1[d] D2[d]} D3[d]: dihedral `d'
E0[e] E1[e]  : vertices in edg `e'

* half edg
see doc/hdg.pdf
h = HDG_VER[v]; h = HDG_EDG[e]; h = HDG_TRI[t]

h = NEXT_HDG[h]
h = FLIP_HDG[h]

v = VER_HDG[h]
e = EDG_HDG[h]
t = TRI_HDG[h]

* boundary

associative arrays with indexes on the boundary:
VER_BND, EDG_BND, HDG_BND

* data

- data/hex :: hexagonal
- data/rbc/icosahedron :: generated using icosahedron tetrasection
- data/rbc/eq :: equilibrated mesh
- data/sph/icosahedron :: generated using icosahedron tetrasection

* directories

- poc :: prove of concept
- lib :: awk libraries
- cmd :: installed user commands
- make :: fragments of make files
- test, test_data :: atest [1] scripts and reference files
- tools :: installed support tools

* lib

- lib/affine.awk :: a list affine matrices
- lib/dtri.awk :: derivates for triangles (dtri_area,...)
- lib/err.awk :: handle error
- lib/gnuplot.awk :: write a format tri.gnuplot can read
- lib/math.awk :: basic math (ctan, rad2deg,...)
- lib/memb.awk :: holds `xx, yy, zz, t0, t1, t2`
- lib/off.awk :: read/write off file
- lib/ply.awk :: read/write ply files
- lib/rbc.awk :: analitical results for "rbc" shape
- lib/setup.awk :: create support structure for mesh
- lib/stat.awk :: statistics (min, max, var, ...)
- lib/transform.awk :: coordiante transformations
- lib/tri.awk :: triangles (area, angles, ...)
- lib/util.awk :: system utilitis (err, msg, shift,...)
- lib/vec.awk :: 3D vectors (dot, cross, ...)
- lib/volume.awk :: volumes (triangle, piramide, prism)
- lib/vtk.awk :: write vtk file

* derivatives

For formulas used in [[file:lib/dtri.awk][dtri.awk]] see [2][3].

* refs

- doc/wardetzky07.pdf
- doc/diff.pdf
- atest https://github.com/slitvinov/atest
