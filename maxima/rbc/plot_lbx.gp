reset

set term postscript enhanced color

set size 0.7, 0.7
set output 'lbx.eps'

sc=0.03;

set xlabel 'x'
set ylabel 'z'

plot 'rbc_maxima.dat' \
u 2:3:($18*sc):($19*sc) w vec t 'lbx'


reset
set term x11
replot
