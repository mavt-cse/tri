reset

set term postscript enhanced color

set size 0.7, 0.7
set output 'normal.eps'

sc=0.1;

set xlabel 'x'
set ylabel 'z'

plot 'rbc_maxima.dat' \
u 2:3:($15*sc):($16*sc) w vec t 'normal'


reset

set term x11

replot
