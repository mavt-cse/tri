install: $M
	@for d in $D; do mkdir -p -- "$L/$$d"; done
	@for f in $M; \
         do t="$L/$$f"; \
	    cp "$$f" "$$t"; \
            if test $$? -ne 0; \
              then echo >&2 '[make/lib.mk] cp failed'; \
              exit 2 ;\
            fi; \
	    echo "install '$$t'"; \
	 done

.PHONY: install
