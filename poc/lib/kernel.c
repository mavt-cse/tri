#include <math.h>
#include <stdio.h>

#include "tri/err.h"
#include "tri/memory.h"
#include "tri/kernel.h"

#define pi (3.141592653589793)
#include "kernel/quintic.h"
#include "kernel/wendland6.h"
#include "kernel/yang.h"
#include "kernel/main.h"
