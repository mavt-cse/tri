#include <math.h>
#include <stdio.h>

#include "tri/err.h"
#include "tri/memory.h"
#include "tri/domain.h"
#include "tri/vec.h"
#include "tri/macro.h"

typedef void (*Wrap)(TriDomain*, /*io*/ double*, double*, double*);
typedef void (*Minus)(TriDomain*, double, double, double,   double, double, double, /**/ double[3]);

struct TriDomain {
    int type;
    Minus minus;
    Wrap wrap;
    double xl, xh, yl, yh, zl, zh;
};

static void wrap(double l, double h, double *px) {
    double x;
    x = *px;
    while (x > h) x -= (h - l);
    while (x < l) x += (h - l);
    *px = x;
}
static void wrap_infinit(/*inout*/ __UNUSED TriDomain *q, __UNUSED double *x, __UNUSED double *y, __UNUSED double *z) {
    /* nothing */
}
static void  wrap_periodicX(TriDomain *q, double *x, __UNUSED double *y, __UNUSED double *z) {
    enum {X};
    wrap(q->xl, q->xh, /**/ x);
}
static void  wrap_periodicXY(TriDomain *q, double *x, double *y, __UNUSED double *z) {
    enum {X, Y};
    wrap(q->xl, q->xh, /**/ x);
    wrap(q->yl, q->yh, /**/ y);
}
static void  wrap_periodicXYZ(TriDomain *q, double *x, double *y, double *z) {
    enum {X, Y, Z};
    wrap(q->xl, q->xh, /**/ x);
    wrap(q->yl, q->yh, /**/ y);
    wrap(q->zl, q->zh, /**/ z);
}

static double min_abs(double x, double y, double z) {
    double x0, y0, z0;
    x0 = fabs(x); y0 = fabs(y); z0 = fabs(z);
    if      (x0 < y0 && x0 < z0) return x;
    else if (y0 < x0 && y0 < z0) return y;
    else                         return z;
}
static double min_dist(double x, double l) {
    double a, b, c;
    a = x - l; b = x; c = x + l;
    return min_abs(a, b, c);
}

static void minus_infinit(__UNUSED TriDomain *q,
                          double ax, double ay, double az,
                          double bx, double by, double bz, /**/
                          double r[3]) {
    enum {X, Y, Z};
    r[X] = ax - bx;
    r[Y] = ay - by;
    r[Z] = az - bz;
}
static void minus_periodicX(TriDomain *q,
                            double ax, double ay, double az,
                            double bx, double by, double bz, /**/
                            double r[3]) {
    enum {X, Y, Z};
    double lx;
    lx = q->xh - q->xl;
    r[X] = min_dist(ax - bx, lx);
    r[Y] = ay - by;
    r[Z] = az - bz;
}
static void minus_periodicXY(TriDomain *q,
                               double ax, double ay, double az,
                               double bx, double by, double bz,
                               double r[3]) {
    enum {X, Y, Z};
    double lx, ly;
    lx = q->xh - q->xl;
    ly = q->yh - q->yl;
    r[X] = min_dist(ax - bx, lx);
    r[Y] = min_dist(ay - by, ly);
    r[Z] = az - bz;
}
static void minus_periodicXYZ(TriDomain *q,
                              double ax, double ay, double az,
                              double bx, double by, double bz,
                              double r[3]) {
    enum {X, Y, Z};
    double lx, ly, lz;

    lx = q->xh - q->xl;
    ly = q->yh - q->yl;
    lz = q->zh - q->zl;

    r[X]  = min_dist(ax - bx, lx);
    r[Y]  = min_dist(ay - by, ly);
    r[Z]  = min_dist(az - bz, lz);
}

enum {DINFINITY, PERIODIC_X, PERIODIC_XY, PERIODIC_XYZ};
static Wrap     wwrap[] = {wrap_infinit, wrap_periodicX, wrap_periodicXY, wrap_periodicXYZ };
static Minus   mminus[] = {minus_infinit, minus_periodicX, minus_periodicXY, minus_periodicXYZ };

void tri_domain_infinit_ini(TriDomain **pq) {
    TriDomain *q;
    MALLOC(1, &q);
    q->minus = mminus[DINFINITY];
    q->wrap  = wwrap[DINFINITY];
    *pq = q;
}

void tri_domain_periodicX_ini(double xl, double xh, TriDomain **pq) {
    TriDomain *q;
    if (xl >= xh) ERR("xl=%g >= xh=%g", xl, xh);

    MALLOC(1, &q);

    q->minus = mminus[PERIODIC_X];
    q->wrap  = wwrap[PERIODIC_X];
    q->xl = xl; q->xh = xh;
    *pq = q;
}

void tri_domain_periodicXY_ini(double xl, double xh, double yl, double yh, TriDomain **pq) {
    TriDomain *q;
    if (xl >= xh) ERR("xl=%g >= xh=%g", xl, xh);
    if (yl >= yh) ERR("yl=%g >= yh=%g", yl, yh);

    MALLOC(1, &q);

    q->minus = mminus[PERIODIC_XY];
    q->wrap     = wwrap[PERIODIC_XY];
    q->xl = xl; q->xh = xh;
    q->yl = yl; q->yh = yh;

    *pq = q;
}

void tri_domain_periodicXYZ_ini(double xl, double xh, double yl, double yh, double zl, double zh, TriDomain **pq) {
    TriDomain *q;
    if (xl >= xh) ERR("xl=%g >= xh=%g", xl, xh);
    if (yl >= yh) ERR("yl=%g >= yh=%g", yl, yh);
    if (zl >= zh) ERR("zl=%g >= zh=%g", zl, zh);

    MALLOC(1, &q);

    q->minus = mminus[PERIODIC_XYZ];
    q->wrap     = wwrap[PERIODIC_XYZ];
    q->xl = xl; q->xh = xh;
    q->yl = yl; q->yh = yh;
    q->zl = zl; q->zh = zh;

    *pq = q;
}
void tri_domain_fin(TriDomain *q) { FREE(q); }

double tri_domain_dist(TriDomain *q, double ax, double ay, double az, double bx, double by, double bz) {
    double s;
    s = tri_domain_sq_dist(q, ax, ay, az, bx, by, bz);
    return sqrt(s);
}

double tri_domain_sq_dist(TriDomain *q,
                          double ax, double ay, double az,
                          double bx, double by, double bz) {
    enum {X, Y, Z};
    double r[3];
    tri_domain_minus(q, ax, ay, az, bx, by, bz, /**/ r);
    return vec_dot(r, r);
}

void tri_domain_wrap(TriDomain *q, double *x, double *y, double *z) { return q->wrap(q, /*io*/ x, y, z); }

void tri_domain_minus(TriDomain *q,
                      double ax, double ay, double az,
                      double bx, double by, double bz, /**/
                      double r[3]) {
    q->minus(q, ax, ay, az, bx, by, bz, /**/ r);
}

