#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <tri/err.h>
#include <tri/memory.h>
#include <tri/kernel.h>
#include <tri/domain.h>
#include <tri/punto.h>
#include <tri/vec.h>
#include <tri/macro.h>

static double Rbody, Abody;
static double NMAX = 5000;
static char File[FILENAME_MAX];
static int N;
static double lo, hi;
static double Sound, Gamma, Mu, dt, Kb;
static TriKernel *kernel;
static TriDomain *domain;
static double Cutoff, Mass;
static double *XX, *YY, *ZZ, *VX, *VY, *VZ, *FX, *FY, *FZ, *RHO,    *LX, *LY, *LZ,  *LVX, *LVY, *LVZ;
static int *NN;

static void write() {
    double *Q[] = {XX, YY, ZZ, RHO, LX, LY, LZ, VX, VY, VZ, NULL};
    static int i = 0;
    char path[FILENAME_MAX];
    snprintf(path, FILENAME_MAX, "o/%05d", i++);
    punto_write(N, Q, path);
}

static double  w(double r) { return tri_kernel_w(kernel, Cutoff, r); }
static double dw(double r) { return tri_kernel_dw(kernel, Cutoff, r); }

static void ini() {
    Abody = 0.0; Rbody = 0.3333;
    
    Sound = 0.25; Mu = 0.0; Kb = 0.1;
    Gamma = 2;
    dt = 1e-4;
    Cutoff = 0.08;
    Mass = 1.0/N;
    lo = -0.5; hi = 0.5;
    tri_kernel_ini(KERNEL_YANG, KERNEL_2D, &kernel);
    //tri_domain_infinit_ini(&domain);
    tri_domain_periodicXY_ini(lo, hi, lo, hi, &domain);
}

static void alloc() {
    MALLOC(NMAX, &XX); MALLOC(NMAX, &YY); MALLOC(NMAX, &ZZ);
    MALLOC(NMAX, &VX); MALLOC(NMAX, &VY); MALLOC(NMAX, &VZ);
    MALLOC(NMAX, &FX); MALLOC(NMAX, &FY); MALLOC(NMAX, &FZ);
    MALLOC(NMAX, &RHO);
    MALLOC(NMAX, &LX); MALLOC(NMAX, &LY); MALLOC(NMAX, &LZ);
    MALLOC(NMAX, &LVX); MALLOC(NMAX, &LVY); MALLOC(NMAX, &LVZ);
    MALLOC(NMAX, &NN);
}

static void fin() {
    tri_kernel_fin(kernel);
    tri_domain_fin(domain);
}

static double dist(int i, int j) {
    return tri_domain_dist(domain,
                           XX[i], YY[i], ZZ[i],
                           XX[j], YY[j], ZZ[j]);
}

static void minus(int i, int j, double r[3]) {
    tri_domain_minus(domain,
                     XX[i], YY[i], ZZ[i],
                     XX[j], YY[j], ZZ[j], /**/ r);
}

static void wrap0(int i) { tri_domain_wrap(domain, &XX[i], &YY[i], &ZZ[i]); }

static void read() {
    double *Q[] = {XX, YY, ZZ, VX, VY, VZ, NULL};
    punto_read(File, /**/ &N, Q);
    if (N > NMAX) ERR("N=%d > NMAX=%d", N, NMAX);
    MSG("get %d particles from '%s'", N, File);
}

static void clear(double *F) {
    int i;
    for (i = 0; i < N; i++) F[i] = 0;
}
static void clear_int(int *F) {
    int i;
    for (i = 0; i < N; i++) F[i] = 0;    
}


static void add_density() {
    int i, j;
    double dr;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            dr = dist(i, j);
            if (dr > Cutoff) continue;
            RHO[i] += w(dr)*Mass;
            if (i != j) NN[i]++;
        }
}

static double pressure(double rho) { return Sound * pow(rho, Gamma); }
static void dpressue(double r[3], double rhoi, double rhoj, /**/ double f[3]) {
    double r0, pi, pj, f0, e[3];
    pi = pressure(rhoi);
    pj = pressure(rhoj);
    r0 = vec_abs(r);
    f0 = -Mass*(pi/(rhoi*rhoi) + pj/(rhoj*rhoj))*dw(r0);
    vec_norm(r, /**/ e);
    vec_scalar(e, f0, /**/ f);
}

static void add_pressure_force() {
    enum {X, Y, Z};
    int i, j;
    double dr, r[3], f[3];
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            if (i == j) continue;
            dr = dist(i, j);
            if (dr > Cutoff) continue;
            minus(i, j, r);
            dpressue(r, RHO[i], RHO[j], /**/ f);
            FX[i] += f[X]; FY[i] += f[Y]; FZ[i] += f[Z];
        }
}

void add_velocity() {
    int i;
    for (i = 0; i < N; i++) {
        VX[i] += dt*FX[i]/Mass;
        VY[i] += dt*FY[i]/Mass;
        VZ[i] += dt*FZ[i]/Mass;
    }
}

static double dlaplace(double dr, double rhoi, double rhoj, double fij) {
    return Mass * fij / (rhoi * rhoj) / dr * dw(dr);
}

void compute_laplace(double F[], double DDF[]) {
    enum {X, Y, Z};
    int i, j;
    double dr;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            if (i == j) continue;
            dr = dist(i, j);
            if (dr > Cutoff) continue;
            DDF[i] += dlaplace(dr, RHO[i], RHO[j], F[i] - F[j]);
        }
}

void compute_laplace_coord(double *LX, double *LY, double *LZ) {
    enum {X, Y, Z};
    int i, j;
    double r[3], dr;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            if (i == j) continue;
            dr = dist(i, j);
            if (dr > Cutoff) continue;
            minus(i, j, /**/ r);
            LX[i] += dlaplace(dr, RHO[i], RHO[j],  r[X]);
            LY[i] += dlaplace(dr, RHO[i], RHO[j],  r[Y]);
            LZ[i] += dlaplace(dr, RHO[i], RHO[j],  r[Z]);
        }
}


void add_position() {
    int i;
    for (i = 0; i < N; i++) {
        XX[i] += dt*VX[i];
        YY[i] += dt*VY[i];
        ZZ[i] += dt*VZ[i];
    }
}

double sq(double x) { return x*x;}
void diag(int step) {
    double E, v[3];
    int i;
    int c, nden_min, nden_max;
    const int BIG = 123456789;
    E = 0;
    for (i = 0; i < N; i++) {
        vec_ini(VX[i], VY[i], VZ[i], /**/ v);
        E += Mass*vec_dot(v, v)/2;
    }
    E /= N;

    nden_min = BIG; nden_max = -BIG;
    for (i = 0; i < N; i++) {
        c = NN[i];
        if (c > nden_max) nden_max = c;
        if (c < nden_min) nden_min = c;
    }
    
    fprintf(stderr, "%d %g [%d %d]\n", step, E, nden_min, nden_max);
}

static void wrap() {
    int i;
    for (i = 0; i < N; i++) wrap0(i);
}

static void add(double s, double *A, double *B) {
    int i;
    for (i = 0; i < N; i++) B[i] += s*A[i];
}

static void add_bending_force() {
    clear(LX); clear(LY); clear(LZ);
    compute_laplace_coord(/**/ LX, LY, LZ);
    add(Kb, LX, FX);
    add(Kb, LY, FY);
    add(Kb, LZ, FZ);
}

static void add_viscose_force() {
    clear(LVX); clear(LVY); clear(LVZ);
    compute_laplace(VX, /**/ LVX);
    compute_laplace(VY, /**/ LVY);
    compute_laplace(VZ, /**/ LVZ);
    add(Mu, LVX, FX);
    add(Mu, LVY, FY);
    add(Mu, LVZ, FZ);
}

static void add_body_force() {
    int i;
    double x, y, r;
    for (i = 0; i < N; i++) {
        x = XX[i]; y = YY[i]; r = sqrt(x*x + y*y);
        if (r < Rbody)
            FZ[i] += Abody*sq(Rbody - r)/Rbody;
    }
}

static void arg(char *v[]) {
    if (v[1] == NULL)
        ERR("needs FILE.in");
    strncpy(File, v[1], FILENAME_MAX); v++;
    MSG("%s", File);
}

int main(__UNUSED int argc, char *argv[]) {
    int i, nstep = 5000;

    alloc();
    arg(argv);
    read();
    ini();
    wrap();
    for (i = 0; /**/ ; i++) {
        clear(FX); clear(FY); clear(FZ); clear(RHO); clear_int(NN);
        add_density();
        add_pressure_force();
        add_bending_force();
        add_viscose_force();
        add_body_force();
        write();
        if (i == nstep) break;
        add_velocity();
        add_position();
        wrap();
        diag(i);
    }
    fin();
}
