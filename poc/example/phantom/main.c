#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <tri/err.h>
#include <tri/memory.h>
#include <tri/kernel.h>
#include <tri/domain.h>
#include <tri/punto.h>
#include <tri/vec.h>
#include <tri/macro.h>
#include <tri/util.h>

#include "imp/decl.h"
#include "imp/alloc.h"
#include "imp/main.h"

