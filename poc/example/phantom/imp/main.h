static void write() {
    static int i = 0;
    static int First = 1;
    char path[FILENAME_MAX];
    double *Q[] = {XX, YY, ZZ, RHO, NX, NY, NZ, TX, TY, TZ, NULL};
    double *P[] = {XXP, YYP, ZZP, FXP, FYP, FZP, RHOP, NULL};
    double *M[] = {XXM, YYM, ZZM, FXM, FYM, FZM, RHOM, NULL};

    if (First) {
        First = 0;
        MSG("write to o/ p/ m/");
        util_mkdir("o"); util_mkdir("p"); util_mkdir("m");
    }

    snprintf(path, FILENAME_MAX, "o/%05d", i);
    punto_write(N, Q, path);
    snprintf(path, FILENAME_MAX, "p/%05d", i);
    punto_write(N, P, path);
    snprintf(path, FILENAME_MAX, "m/%05d", i);
    punto_write(N, M, path);
    i++;
}

static double  w(double r) { return tri_kernel_w(kernel, Cutoff, r); }
static double dw(double r) { return tri_kernel_dw(kernel, Cutoff, r); }

static void ini() {
    Rbody = 0.333; Abody = 1e1;
    Sound = 1.0; Mu = 0.0;
    Gamma = 2;
    dt = 1e-4;
    Cutoff = 0.08; Skin = Cutoff;
    Mass = 1.0/N;
    lo = -0.5; hi = 0.5;
    tri_kernel_ini(KERNEL_YANG, KERNEL_2D, &kernel);
    //tri_domain_infinit_ini(&domain);
    tri_domain_periodicXY_ini(lo, hi, lo, hi, &domain);
}

static void fin() {
    tri_kernel_fin(kernel);
    tri_domain_fin(domain);
}

static double dist(int i, int j, double *xx, double *yy, double *zz) {
    return tri_domain_dist(domain,
                           xx[i], yy[i], zz[i],
                           xx[j], yy[j], zz[j]);
}

static void minus(int i, int j, double *xx, double *yy, double *zz, double r[3]) {
    tri_domain_minus(domain,
                     xx[i], yy[i], zz[i],
                     xx[j], yy[j], zz[j], /**/ r);
}

static void norm(double *xx, double *yy, double *zz) {
    int i;
    double n[3], e[3];
    for (i = 0; i < N; i++) {
        vec_ini(xx[i], yy[i], zz[i], /**/ n);
        vec_norm(n, /**/ e);
        vec_coord(e, /**/ &xx[i], &yy[i], &zz[i]);
    }
}

static void read() {
    double *Q[] = {XX, YY, ZZ, VX, VY, VZ, NX, NY, NZ, NULL};
    punto_read(File, /**/ &N, Q);
    if (N > NMAX) ERR("N=%d > NMAX=%d", N, NMAX);
    MSG("get %d particles from '%s'", N, File);
}

static void clear(double *F) {
    int i;
    for (i = 0; i < N; i++) F[i] = 0;
}
static void clear_int(int *F) {
    int i;
    for (i = 0; i < N; i++) F[i] = 0;
}
static void copy(double *a, /**/ double *b) {
    int i;
    for (i = 0; i < N; i++) b[i] = a[i];
}

static void add_density(double *XX, double *YY, double *ZZ, /**/ double *RHO) {
    int i, j;
    double dr;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            dr = dist(i, j, XX, YY, ZZ);
            if (dr > Cutoff) continue;
            RHO[i] += w(dr)*Mass;
            if (i != j) NN[i]++;
        }
}

static double pressure(double rho) { return Sound * pow(rho, Gamma); }
static void dpressue(double r[3], double rhoi, double rhoj, /**/ double f[3]) {
    double r0, pi, pj, f0, e[3];
    pi = pressure(rhoi);
    pj = pressure(rhoj);
    r0 = vec_abs(r);
    f0 = -Mass*(pi/(rhoi*rhoi) + pj/(rhoj*rhoj))*dw(r0);
    vec_norm(r, /**/ e);
    vec_scalar(e, f0, /**/ f);
}

static void add_pressure_force(double *xx, double *yy, double *zz, const double *rho,
                               /**/ double *fx, double *fy, double *fz) {
    enum {X, Y, Z};
    int i, j;
    double dr, r[3], f[3];
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            if (i == j) continue;
            dr = dist(i, j, xx, yy, zz);
            if (dr > Cutoff) continue;
            minus(i, j, xx, yy, zz, r);
            dpressue(r, rho[i], rho[j], /**/ f);
            fx[i] += f[X]; fy[i] += f[Y]; fz[i] += f[Z];
        }
}

void add_velocity(const double *fx,
                  const double *fy,
                  const double *fz) {
    int i;
    for (i = 0; i < N; i++) {
        VX[i] += dt*fx[i]/Mass;
        VY[i] += dt*fy[i]/Mass;
        VZ[i] += dt*fz[i]/Mass;
    }
}

void sub_velocity(const double *fx,
                  const double *fy,
                  const double *fz) {
    int i;
    for (i = 0; i < N; i++) {
        VX[i] -= dt*fx[i]/Mass;
        VY[i] -= dt*fy[i]/Mass;
        VZ[i] -= dt*fz[i]/Mass;
    }
}

static double dlaplace(double dr, double rhoi, double rhoj, double fij) {
    return Mass * fij / (rhoi * rhoj) / dr * dw(dr);
}

void compute_laplace(double F[], double DDF[]) {
    enum {X, Y, Z};
    int i, j;
    double dr;
    for (i = 0; i < N; i++)
        for (j = 0; j < N; j++) {
            if (i == j) continue;
            dr = dist(i, j, XX, YY, ZZ);
            if (dr > Cutoff) continue;
            DDF[i] += dlaplace(dr, RHO[i], RHO[j], F[i] - F[j]);
        }
}

void add_position() {
    int i;
    for (i = 0; i < N; i++) {
        XX[i] += dt*VX[i];
        YY[i] += dt*VY[i];
        ZZ[i] += dt*VZ[i];
    }
}

double sq(double x) { return x*x;}
void diag(int step) {
    double E, v[3];
    int i;
    int c, nden_min, nden_max;
    const int BIG = 123456789;
    E = 0;
    for (i = 0; i < N; i++) {
        vec_ini(VX[i], VY[i], VZ[i], /**/ v);
        E += Mass*vec_dot(v, v)/2;
    }
    E /= N;

    nden_min = BIG; nden_max = -BIG;
    for (i = 0; i < N; i++) {
        c = NN[i];
        if (c > nden_max) nden_max = c;
        if (c < nden_min) nden_min = c;
    }

    fprintf(stderr, "%d %g [%d %d]\n", step, E, nden_min, nden_max);
}

static void wrap(double *XX, double *YY, double *ZZ) {
    int i;
    for (i = 0; i < N; i++)
        tri_domain_wrap(domain, &XX[i], &YY[i], &ZZ[i]);
}

static void add(double s, double *A, double *B) {
    int i;
    for (i = 0; i < N; i++) B[i] += s*A[i];
}

static void add_viscose_force() {
    clear(LVX); clear(LVY); clear(LVZ);
    compute_laplace(VX, /**/ LVX);
    compute_laplace(VY, /**/ LVY);
    compute_laplace(VZ, /**/ LVZ);
    add(Mu, LVX, FX);
    add(Mu, LVY, FY);
    add(Mu, LVZ, FZ);
}

static void phantom() {
    int i;
    double r[3], n[3], dr[3], rp[3], rm[3];
    for (i = 0; i < N; i++) {
        vec_ini(XX[i], YY[i], ZZ[i], /**/ r);
        vec_ini(NX[i], NY[i], NZ[i], /**/ n);
        vec_scalar(n, Skin, /**/ dr);
        vec_plus(r,  dr,  /**/ rp);
        vec_minus(r, dr,  /**/ rm);
        vec_coord(rp, /**/ &XXP[i], &YYP[i], &ZZP[i]);
        vec_coord(rm, /**/ &XXM[i], &YYM[i], &ZZM[i]);
    }
}

static void compute_torque() {
    int i;
    double n[3], fp[3], fm[3], tp[3], tm[3], t0[3], t[3];
    for (i = 0; i < N; i++) {
        vec_ini(NX[i], NY[i], NZ[i], /**/ n);
        vec_ini(FXP[i], FYP[i], FZP[i], /**/ fp);
        vec_ini(FXM[i], FYM[i], FZM[i], /**/ fm);
        vec_cross(n, fp, /**/ tp);
        vec_cross(n, fm, /**/ tm);
        vec_minus(tp, tm, /**/ t0);
        vec_scalar(t0, Skin, /**/ t);
        vec_coord(t, /**/ &TX[i], &TY[i], &TZ[i]);
    }
}

static void add_omega() {
    int i;
    double I;
    I = 2*Mass*Skin*Skin;
    for (i = 0; i < N; i++) {
        WX[i] += dt*TX[i]/I;
        WY[i] += dt*TY[i]/I;
        WZ[i] += dt*TZ[i]/I;
    }
}

static void compute_normal() {
    int i;
    double w[3], n[3], n0[3], v[3], dn[3];
    for (i = 0; i < N; i++) {
        vec_ini(WX[i], WY[i], WZ[i], /**/ w);
        vec_ini(NX[i], NY[i], NZ[i], /**/ n0);
        vec_scalar(n0, Skin, /**/ n);
        vec_cross(w, n, /**/ v);
        vec_scalar(v, dt, /**/ dn);

        vec_plus(n, dn, /**/ n0);
        vec_norm(n0, /**/ n);
        vec_coord(n, &NX[i], &NY[i], &NZ[i]);
        //vec_printf(n, stderr, "[%g %g %g]\n");
    }
}


static void add_bodyforce() {
    int i;
    double r;
    for (i = 0; i < N; i++) {
        r = sqrt(sq(XX[i]) + sq(YY[i]));
        if (r < Rbody)
            FZ[i] += Abody; // * sq(Rbody - r)/sq(Rbody);
    }
}

static void arg(char *v[]) {
    if (v[1] == NULL)
        ERR("needs FILE.in");
    strncpy(File, v[1], FILENAME_MAX); v++;
    MSG("%s", File);
}

int main(__UNUSED int argc, char *argv[]) {
    int i, nstep = 50000;

    alloc();
    arg(argv);

    read();
    norm(NX, NY, NZ);
    copy(NX, /**/ NX0); copy(NY, /**/ NY0); copy(NZ, /**/ NZ0);
    clear(WX); clear(WY); clear(WZ);
    ini();

    for (i = 0; /**/ ; i++) {
        clear_int(NN);
        clear(FX); clear(FY); clear(FZ); clear(RHO);
        clear(FXP); clear(FYP); clear(FZP); clear(RHOP);
        clear(FXM); clear(FYM); clear(FZM); clear(RHOM);

        wrap(XX, YY, ZZ);
        phantom(); wrap(XXP, YYP, ZZP); wrap(XXM, YYM, ZZM);

        add_density(XX, YY, ZZ, /**/ RHO);
        add_pressure_force(XX, YY, ZZ, RHO, /**/ FX, FY, FZ);
        add_bodyforce();

        add_density(XXP, YYP, ZZP, /**/ RHOP);
        add_density(XXM, YYM, ZZM, /**/ RHOM);
        add_pressure_force(XXP, YYP, ZZP, RHOP, /**/
                           FXP, FYP, FZP);
        add_pressure_force(XXM, YYM, ZZM, RHOM, /**/
                           FXM, FYM, FZM);
        add_viscose_force();
        if (i == nstep) break;
        compute_torque();
        add_omega();
        compute_normal();

        add_velocity(FX, FY, FZ);
        add_velocity(FXP, FYP, FZP);
        sub_velocity(FXM, FYM, FZM);
        add_position(); wrap(XX,   YY, ZZ);

        write();
        diag(i);
        clear(VX); clear(VY); clear(VZ); clear(WX); clear(WY); clear(WZ);
    }
    fin();
}
