static void alloc() {
    int n;
    n = NMAX;
    MALLOC(n, &XX); MALLOC(n, &YY); MALLOC(n, &ZZ);
    MALLOC(n, &VX); MALLOC(n, &VY); MALLOC(n, &VZ);
    MALLOC(n, &FX); MALLOC(n, &FY); MALLOC(n, &FZ);
    MALLOC(n, &RHO);
    MALLOC(n, &LVX); MALLOC(n, &LVY); MALLOC(n, &LVZ);
    MALLOC(n, &NN);

    MALLOC(n, &XXP); MALLOC(n, &YYP); MALLOC(n, &ZZP);
    MALLOC(n, &FXP); MALLOC(n, &FYP); MALLOC(n, &FZP);
    MALLOC(n, &RHOP);

    MALLOC(n, &XXM); MALLOC(n, &YYM); MALLOC(n, &ZZM);
    MALLOC(n, &FXM); MALLOC(n, &FYM); MALLOC(n, &FZM);
    MALLOC(n, &RHOM);

    MALLOC(n, &NX); MALLOC(n, &NY); MALLOC(n, &NZ);
    MALLOC(n, &NX0); MALLOC(n, &NY0); MALLOC(n, &NZ0);

    
    MALLOC(n, &WX); MALLOC(n, &WY); MALLOC(n, &WZ);
    MALLOC(n, &TX); MALLOC(n, &TY); MALLOC(n, &TZ);
}
