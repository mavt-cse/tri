static double NMAX = 5000;
static char File[FILENAME_MAX];
static int N;
static double lo, hi;
static double Sound, Gamma, Mu, dt, Skin, Rbody, Abody;
static TriKernel *kernel;
static TriDomain *domain;
static double Cutoff, Mass;
static double *XX, *YY, *ZZ, *VX, *VY, *VZ, *FX, *FY, *FZ, *RHO;
static double *LVX, *LVY, *LVZ; /* laplacians */

static double *NX,   *NY,  *NZ; /* normals */
static double *NX0, *NY0, *NZ0; /* initial normals */

static double *WX, *WY, *WZ; /* angular velocity */
static double *TX, *TY, *TZ; /* torque */

static double *XXP, *YYP, *ZZP, *FXP, *FYP, *FZP, *RHOP; /* phantom meshes (plus and minus) */
static double *XXM, *YYM, *ZZM, *FXM, *FYM, *FZM, *RHOM;

static int *NN; /* number of neighbors */
