#include <stdio.h>
#include <tri/err.h>
#include <tri/punto.h>

#define N (3)

static double XX[N];
static double YY[N];
static double ZZ[N];
static double *queue[] = {XX, YY, ZZ, NULL};

int main(int argc, const char *argv[]) {
    const char *i, *o;
    int n;
    if (argv[1] == NULL)
        ERR("needs FILE.in");
    i = argv[1]; argv++;

    if (argv[1] == NULL)
        ERR("needs FILE.out");
    o = argv[1]; argv++;
    
    punto_read(i, /**/ &n, queue);
    MSG("n = %d", n);
    MSG("writing: '%s'", o);
    punto_write(n, queue, o);
}
