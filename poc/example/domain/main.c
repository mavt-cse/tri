#include <math.h>
#include <stdio.h>
#include <string.h>

#include <tri/err.h>
#include <tri/domain.h>

#define SIZE (2048)
enum {WRAP, DIST, SQ_DIST, MINUS};

static TriDomain *domain;
static double xl, xh, yl, yh, zl, zh;
static double ax, ay, az, bx, by, bz;

static void infinity()    { tri_domain_infinit_ini(&domain); }
static void periodicX()   { tri_domain_periodicX_ini(xl, xh, &domain); }
static void periodicXY()  { tri_domain_periodicXY_ini(xl, xh, yl, yh, &domain); }
static void periodicXYZ() { tri_domain_periodicXYZ_ini(xl, xh, yl, yh, zl, zh, &domain); }

static void fin()    { tri_domain_fin(domain); }
static void   wrap() { tri_domain_wrap(domain, &ax, &ay, &az); }
static double dist() { return tri_domain_dist(domain, ax, ay, az, bx, by, bz); }
static double sq_dist() { return tri_domain_sq_dist(domain, ax, ay, az, bx, by, bz); }
static void minus(double r[3]) { tri_domain_minus(domain, ax, ay, az, bx, by, bz, /**/ r); }

static int eq(const char *a, const char *b) { return strncmp(a, b, SIZE) == 0; }

static void read_domain() {
    int n;
    char msg[SIZE];
    n = scanf("%lf %lf  %lf %lf  %lf %lf", &xl, &xh,  &yl, &yh,   &zl, &zh);
    if (n != 2*3)
        ERR("expect [xl xh  yl yh   zl zh]");
    
    if (scanf("%s", msg) != 1)
        ERR("fail to read domain type");
    if (eq(msg, "infinity")) infinity();
    else if (eq(msg, "periodicX"))   periodicX();
    else if (eq(msg, "periodicXY"))  periodicXY();
    else if (eq(msg, "periodicXYZ")) periodicXYZ();
    else ERR("unknown domain type '%s'", msg);
}

static int read_point(int *paction) {
    int action, n;
    char msg[SIZE];    
    if (scanf("%s", msg) != 1) return 0;
    if (eq(msg, "wrap")) action = WRAP;
    else if (eq(msg, "dist")) action = DIST;
    else if (eq(msg, "sq_dist")) action = SQ_DIST;
    else if (eq(msg, "minus"))   action = MINUS;
    else ERR("unknown action '%s'", msg);
    if (action == WRAP) {
        n = scanf("%lf %lf %lf", &ax, &ay, &az);
        if (n != 3) ERR("expect [ax ay az]");
    } else {
        n = scanf("%lf %lf %lf   %lf %lf %lf", &ax, &ay, &az, &bx, &by, &bz);
        if (n != 2*3) ERR("expect [ax ay az   bx by bz]");
    }
    *paction = action;
    return 1;
}

int main() {
    enum {X, Y, Z};
    double r[3];
    int action;
    read_domain();
    while (read_point(&action)) {
        switch (action) {
        case WRAP:
            wrap();
            printf("%g %g %g\n", ax, ay, az);
            break;
        case DIST:
            printf("%g\n", dist());
            break;
        case SQ_DIST:
            printf("%g\n", sq_dist());
            break;
        case MINUS:
            minus(r);
            printf("%g %g %g\n", r[X], r[Y], r[Z]);
            break;            
        }
    }

    fin();
}
