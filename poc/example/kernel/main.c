#include <math.h>
#include <stdio.h>

#include <tri/err.h>
#include <tri/memory.h>
#include <tri/kernel.h>

static const double pi = 3.141592653589793;

int main() {
    TriKernel *kernel;
    double c, lo, hi, dx, x, sum1d, sum2d, sum3d, d;
    int i, j, k, n;
    double *w, *dw;

    c = 2.0; n = 1000;
    lo = -2*c; hi = 2*c;

    MALLOC(n, &w); MALLOC(n, &dw);
    tri_kernel_ini(KERNEL_QUINTIC, KERNEL_3D, &kernel);
    dx = (hi - lo)/(n - 1);
    for (i = 0; i < n; i++) {
        x = lo + i*dx;
        w[i]   =   tri_kernel_w(kernel, c, x);
        dw[i]  =  tri_kernel_dw(kernel, c, x);
    }

    sum1d = sum2d = sum3d = 0;
    for (i = 0; i < n; i++) {
        x = lo + i*dx;
        sum1d += w[i];
        sum2d += fabs(x)*w[i];
        sum3d += fabs(x)*fabs(x)*w[i];
    }
    fprintf(stderr, "I: %g %g %g\n", sum1d*dx, 2*pi*sum2d*dx/2, 4*pi*sum3d*dx/2);

    for (j = 1; j < n - 1; j++) {
        x = lo + j*dx;
        i = j - 1;
        k = j + 1;
        d  = (w[k] - w[i])/(2*dx);
        printf("%g %g %g %g\n", x, w[i], dw[i], d);
    }

    tri_kernel_fin(kernel);
    FREE(w); FREE(dw);
}
