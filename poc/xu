. tri.util
FIFO=/tmp/basic.$$
if ! e mkfifo $FIFO
then err "fail to create fifo: '$FIFO'"
fi
trap 'e rm -f $FIFO; exit 2' 1 2 3 15
File="$1"; shift

tri.awk util off setup stat tri dtri math vec err surf x  -e '
BEGIN {
    ini()
    arg()
    off_read(File); setup()
    xu()
    write()
}

function ini() {
    prog = "poc/xu"
}
function arg() {
    if (noarg()) err("needs OFF file")
    File = ARGV[1]; shift()
    if (noarg()) err("needs FIFO")
    FIFO = ARGV[1]; shift()
    PIPE = sprintf("tee .log | tri.basis normal > %s", quote(FIFO))
}

function write_rhs(p, rhs,   i) {
    print util_nelem(rhs) | p
    for (i = 0; i in rhs; i++)
	printf "%.16e\n", rhs[i] | p
}

function write_matrix(p, B,   l, k, n) {
    print util_nelem(B) | p
    for (k = 0; (k, 0) in B; k++)
	for (l = 0; (k, l) in B; l++)
	    printf "%.16e\n", B[k,l] | p
}

function read_array(f, a,   r, n, x) {
    delete a
    r = getline n < f
    if (r < 0) err(sprintf("fail to read from '%s'", f))
    if (!integerp(n))
	err(sprintf("not an integer '%s'", n))
    for (i = 0; i < n; i++) {
	r = getline x < f
	if (r < 0) err(sprintf("fail to read from '%s'", f))
	a[i] = x
    }
}

function xu(   v, pipe, r, h) {
    for (v = 0; v < NV; v++) {
	if (v in VER_BND) continue
	h = xu0(v, rhs, matrix)
	printf "%.16e\n", h | PIPE
	write_matrix(PIPE, matrix)
	write_rhs(PIPE, rhs)
	read_array(FIFO, nn)
	vec_set(nn, v, nx, ny, nz)
	if (v % 10 == 0)
	    msg(sprintf("%5d / %d", v, NV))
    }
    printf "e\n" | PIPE
}

function push_matrix(k, u, v, B,   l) {
    l = 0
    B[k, l++] = 1
    B[k, l++] = u
    B[k, l++] = v
    B[k, l++] = 1/2*u^2
    B[k, l++] = u*v
    B[k, l++] = 1/2*v^2
}

function push_rhs(k, a,   R,   i, X, Y, Z) {
    X = 0; Y = 1; Z = 2
    i = 3*k
    R[i++] = a[X]; R[i++] = a[Y]; R[i++] = a[Z]
}

function get_tri(v, rank, a, b, c, r,  i, j, k) {
    i = R0[v,rank]; j = R1[v,rank]; k = R2[v,rank]
    get3(i, j, k, a, b, c)
    vec_minus(a, b,  r)
}

function xu0(v,   RHS, MATRIX, rank, a, b, c, ang, norm, rr, r, q, th, th0, u, pi, k) {
    pi = 3.141592653589793

    vec_get(v, XX, YY, ZZ, a)
    k = 0
    push_rhs(k++, a, RHS)
    for (rank = 0; (v, rank) in R0; rank++) {
	get_tri(v, rank,   a, b, c, r)
	push_rhs(k++, b, RHS)
	rr[rank] = vec_abs(r)
	ang[rank] = tri_angle0(b, a, c)
	norm += ang[rank]
    }

    k = 0
    push_matrix(k++, 0, 0,  MATRIX)
    th = th0 = 0
    for (rank = 0; rank in rr; rank++) {
	q = rr[rank]
	th0 = 2*pi*th/norm
	u = q*cos(th0); v = q*sin(th0)
	push_matrix(k++, u, v,   MATRIX)
	th += ang[rank]
    }
    return stat_max(rr)
}

function write(   v) {
    for (v = 0; v in XX; v++) {
	if (v in VER_BND) continue
	x = XX[v]; y = YY[v]; z = ZZ[v]
	print x, y, z, nx[v], ny[v], nz[v]
    }
}

' -- "$File" "$FIFO"

e rm -f $FIFO
